<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NSU CIICT | Quick Access</title>
    <link rel="icon" href="app/lib/qaL.png">

     <!-- Bootstrap core CSS -->
    <link href="app/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- My CSS -->
    <link href="app/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="app/css/w3.css">

	<style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
      .foot-nav{
        background-color: #ffff67;
        padding: 5px;
      }
      .margin{
        margin-left: 2em;
      }

    </style>

   </head>
  <body style="background-color: #ffffd3;">

    <!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top"  style="background-color: #ffff67;">
    	<div class="container">
          <a class="navbar-brand" href=""><img src="app/lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access 
	      </a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	      </button>
	      <div class="collapse navbar-collapse text-center" id="navbarResponsive">
        	  <ul class="navbar-nav ml-auto nav-menu" id="qa-nav">
        	  	<li ><a class="nav-link"  href="#">Home</a></li>
        	  	<li ><a class="nav-link"  href="#about">About</a></li>
              <li ><a class="nav-link"  href="#contactus">Contact Us</a></li>
              <li ><a class="nav-link"  href="app/login">Login</a></li>
        	  </ul>
    	  </div>
    	</div>
	</nav>
  <header role="banner" style="background-image: url(app/lib/deskiStock_000000520125Small.jpg); color: blue; text-shadow: 1px 2px 3px #ffff67; height: 300px; background-size: cover; background-repeat: no-repeat; background-position: center center;">
    <div class="text-center"><br><br><br>
      <h2>Welcome to <h1 style="font-family: Segoe Print">NSU-CIICT Quick Access</h1></h2><br>
      <a class="qa-btn-default col-lg" id="qa-btn" style="text-decoration: none;" href="app/login#signup"><b>SIGN UP</b></a>
    </div>
  </header>

  <section class="container">
    <div class="row text-center">
      <div class="col-lg-1 col-md-1 col-sm-1"></div>
      <div class="col-lg-2 col-md-5 col-sm-5"><br>
        <img style="width: 100%;" src="app/lib/nsu_logo.png">
      </div>
      <div class="col-lg-2 col-md-5 col-sm-5"><br>
        <img style="width: 100%;" src="app/lib/ciict_logo.png">
      </div>
      <div class="col-lg-6 text-left" id="about"><br><br>
        <p class="m-3">
          NSU (Naval State University)<br>
          CIICT (College of Industrial Information and Communication Technology<br>
          <ul class="m-4"><h5>What we have?</h5>
            <li>Students may able to look for their Class Schedule</li>
            <li>Students may able to look for their Grades</li>
            <li>Students may able to ask about their studies</li>
            <li>ICT Library</li>
          </ul> 
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-1"></div>
      <div class="col-lg-9">
        <p class="m-2"><span class="margin"></span> During recent years, navigation using web pages have gained a considerable importance not only among the academia but also amongst students, parents, industry and businesses, but the pioneers of the web were researchers and university teachers.</p>
        <p class="m-2"><span class="margin"></span> Why have department’s website? Today, Web is not only an information resource but also it is becoming an automated tool. This will reflect the department which has a background study about ICT.</p>
        <br>
      </div>
    </div>
  </section>

	<div class="content">
	</div>

  <footer id="contactus" style="background-color: #282c2d; color: #f9f9f9; background-image: url(app/lib/dashed.png); padding: 5px;">
    <h2 class="text-center">Contact Us</h2>
    <div class="container">
      <div class="row">
        <iframe class="col-lg-4" height="275" id="gmap_canvas" src="https://maps.google.com/maps?q=naval%20state%20university%20naval%2Cbiliran&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        <div class="col-lg-4">
          Email : <h6 class="margin">qa-team@gmail.com</h6>
          Address : <h6 class="margin">Caraycaray, Naval, Biliran</h6>
          Phone Number : <h6 class="margin">+639505667759</h6>
        </div>
        <div class="col-lg-4">
          
        </div>
      </div>
    </div>
  </footer>
  <div class="foot-nav text-right">
    All Rights Reserve <u>NSU-CIICT Quick Access</u> &copy; 2018, <a href="https://jennitang.com" target="_blank">jenniTang</a> works.
  </div>
	<!-- Bootstrap core JavaScript -->
	<script src="app/css/jquery/jquery-3.2.1.slim.min.js"></script>
	<script src="app/css/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>