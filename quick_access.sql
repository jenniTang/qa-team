-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2019 at 05:46 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quick_access`
--

-- --------------------------------------------------------

--
-- Table structure for table `qa_add_info`
--

CREATE TABLE `qa_add_info` (
  `add_info_id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `section` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_add_info`
--

INSERT INTO `qa_add_info` (`add_info_id`, `course_id`, `section`, `year`, `status`) VALUES
(14, 1, 'A', 'Fourth Year', 'Regular'),
(15, 1, 'A', 'Fourth Year', 'Regular'),
(16, 1, 'A', 'First Year', 'Regular'),
(18, 1, 'B', 'Second Year', 'Regular'),
(19, 1, 'B', 'Fourth Year', 'Regular'),
(20, 1, 'A', 'First Year', 'Regular'),
(21, 2, 'A', 'First Year', 'Regular'),
(23, 1, 'A', 'First Year', 'Irregular');

-- --------------------------------------------------------

--
-- Table structure for table `qa_announce`
--

CREATE TABLE `qa_announce` (
  `announce_id` int(10) NOT NULL,
  `announce` text NOT NULL,
  `announce_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_announce`
--

INSERT INTO `qa_announce` (`announce_id`, `announce`, `announce_date`) VALUES
(1, '<p>Hello Fellas</p>\r\n', '2019-02-27 07:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `qa_books`
--

CREATE TABLE `qa_books` (
  `book_id` int(20) NOT NULL,
  `sub_id` int(10) NOT NULL,
  `book_name` varchar(150) NOT NULL,
  `book_author` varchar(100) NOT NULL,
  `book_description` text NOT NULL,
  `book_file` varchar(250) NOT NULL,
  `book_permit` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_books`
--

INSERT INTO `qa_books` (`book_id`, `sub_id`, `book_name`, `book_author`, `book_description`, `book_file`, `book_permit`) VALUES
(16, 1, 'Modern Digital Electronics', 'Xie Songyun', 'Digital Signal, Basic Digital Circuits, NAND and NOR Operation, Exclusive-OR Operation, Boolean Algebra\r\n&& Examples of IC Gates', '../../library/AI BOOK.pdf', 'today'),
(17, 4, 'Data Communications and Networking', 'Behrouz A. Forouzan', 'This text is designed for students with little or no background in telecommunications\r\nor data communications. For this reason, we use a bottom-up approach. With this\r\napproach, students learn first about data communications (lower layers) before learning\r\nabout networking (upper layers).', '../../library/Data Communication.pdf', 'Fourth Edition'),
(18, 5, 'Constraint Satisfaction Problem', 'Marc Erich Latoschik', 'Constraint study', '../../library/Constraint Satisfaction Problem.pdf', 'next month'),
(19, 5, 'backtracking search', 'Hojjat Ghaderi', 'testing', '../../library/backtracking search.pdf', 'free');

-- --------------------------------------------------------

--
-- Table structure for table `qa_class`
--

CREATE TABLE `qa_class` (
  `class_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `sub_id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `year` varchar(50) NOT NULL,
  `section` varchar(10) NOT NULL,
  `class_time` varchar(150) NOT NULL,
  `class_day` varchar(150) NOT NULL,
  `class_room` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_class`
--

INSERT INTO `qa_class` (`class_id`, `user_id`, `sub_id`, `course_id`, `year`, `section`, `class_time`, `class_day`, `class_room`) VALUES
(6, 6, 2, 1, 'First Year', 'A', '2:00pm to 3:30pm', 'Thursday', 'lab1'),
(7, 6, 1, 1, 'Third Year', 'B', '12:00 - 2:00pm', 'Thursday', 'lab2');

-- --------------------------------------------------------

--
-- Table structure for table `qa_courses`
--

CREATE TABLE `qa_courses` (
  `course_id` int(10) NOT NULL,
  `course_name` varchar(150) NOT NULL,
  `major` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_courses`
--

INSERT INTO `qa_courses` (`course_id`, `course_name`, `major`) VALUES
(1, 'BS in Computer Science', ''),
(2, 'BS in Information System', NULL),
(3, 'BS in Industrial Technology', 'Automotive'),
(4, 'BS in Industrial Technology', 'Drafting'),
(5, 'BS in Industrial Technology', 'Electronics'),
(6, 'BS in Industrial Technology', 'Foods'),
(7, 'BS in Industrial Technology', 'Garments');

-- --------------------------------------------------------

--
-- Table structure for table `qa_forum_comment`
--

CREATE TABLE `qa_forum_comment` (
  `forum_comment_id` int(10) NOT NULL,
  `forum_topic_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `comment` text NOT NULL,
  `comment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_forum_comment`
--

INSERT INTO `qa_forum_comment` (`forum_comment_id`, `forum_topic_id`, `user_id`, `comment`, `comment_date`) VALUES
(27, 21, 2, 'that is just so simple', '2018-10-19 00:57:26'),
(28, 21, 2, 'asdasd', '2018-10-22 13:11:01'),
(29, 24, 2, '', '2019-02-14 19:34:26'),
(30, 24, 2, 'lol', '2019-02-14 19:34:36'),
(31, 24, 6, 'hahhaha', '2019-02-26 15:36:39'),
(32, 24, 6, '???', '2019-02-26 15:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `qa_forum_reply`
--

CREATE TABLE `qa_forum_reply` (
  `forum_reply_id` int(10) NOT NULL,
  `forum_comment_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply` text NOT NULL,
  `reply_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_forum_reply`
--

INSERT INTO `qa_forum_reply` (`forum_reply_id`, `forum_comment_id`, `user_id`, `reply`, `reply_date`) VALUES
(18, 27, 2, 'how?', '2018-10-19 00:59:48'),
(19, 27, 2, 'sd', '2018-10-22 13:10:48'),
(20, 30, 6, 'was really fun isnt it?', '2019-02-26 15:37:16'),
(21, 32, 6, 'aDAFEAFjfhyuy', '2019-02-26 15:40:44'),
(22, 30, 6, 'klhgtdrdy', '2019-02-26 15:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `qa_forum_topic`
--

CREATE TABLE `qa_forum_topic` (
  `forum_topic_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `topic` text NOT NULL,
  `topic_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_forum_topic`
--

INSERT INTO `qa_forum_topic` (`forum_topic_id`, `user_id`, `topic`, `topic_date`) VALUES
(21, 2, 'How to make a multiplication table in programming c', '2018-10-19 00:55:28'),
(22, 2, 'Hello world, hello QA', '2018-10-22 13:12:24'),
(23, 2, '<p>kbvtrdfcvcrdtfkurcfgc</p>\r\n', '2018-12-04 09:03:01'),
(24, 2, '<p>CS</p>\r\n', '2019-02-14 19:34:16'),
(29, 27, '<p>Hello World</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2019-02-26 18:00:18');

-- --------------------------------------------------------

--
-- Table structure for table `qa_profile`
--

CREATE TABLE `qa_profile` (
  `profile_id` int(10) NOT NULL,
  `fname` varchar(150) NOT NULL,
  `mname` varchar(150) DEFAULT NULL,
  `lname` varchar(150) NOT NULL,
  `add_info_id` int(10) DEFAULT NULL,
  `profile_image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_profile`
--

INSERT INTO `qa_profile` (`profile_id`, `fname`, `mname`, `lname`, `add_info_id`, `profile_image`) VALUES
(1, 'jenni', 'Montemor', 'Tang', NULL, ''),
(5, 'Cheryl Ann', 'Villaflor', 'Tadefa', NULL, ''),
(16, 'Pablito Jr.', '', 'Sales', 14, ''),
(17, 'Ronald Rey', '', 'Ventulan', 15, ''),
(18, 'Jenni', '', 'Tang', 16, ''),
(22, 'Jonathan', 'Narrido', 'Cabalquinto', NULL, ''),
(24, 'Chad Ryan', 'Balo', 'Gadugdug', 18, ''),
(25, 'Faith Roxanne', 'Olea', 'Sales', 19, ''),
(28, 'Faith Roxanne', '', 'Sales', 20, 'lib/jenn@mail.jemala.jpg'),
(29, 'sensei', '', 'Olea', 21, 'lib/new@mail.com.jpg'),
(30, 'Stephen Janseen', 'Dela Pena', 'Balo', 23, 'lib/stephen_janz93@yahoo.com.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `qa_stud_ga`
--

CREATE TABLE `qa_stud_ga` (
  `stud_ga_id` int(11) NOT NULL,
  `class_id` int(10) NOT NULL,
  `sub_enrolled_id` int(10) NOT NULL,
  `user_id` int(20) NOT NULL,
  `sub_id` int(20) NOT NULL,
  `sub_grade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_stud_ga`
--

INSERT INTO `qa_stud_ga` (`stud_ga_id`, `class_id`, `sub_enrolled_id`, `user_id`, `sub_id`, `sub_grade`) VALUES
(25, 6, 24, 15, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `qa_subjects`
--

CREATE TABLE `qa_subjects` (
  `sub_id` int(20) NOT NULL,
  `sub_code` varchar(150) NOT NULL,
  `sub_name` varchar(150) NOT NULL,
  `sub_unit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_subjects`
--

INSERT INTO `qa_subjects` (`sub_id`, `sub_code`, `sub_name`, `sub_unit`) VALUES
(1, 'CS423', 'Compiler Design', 4),
(2, 'CS112', 'Programming C', 5),
(3, 'CS213', 'Discrete Structure', 3),
(4, 'CS311', 'Data Communication', 3),
(5, 'CS412', 'Artificial Intelegence', 3),
(6, 'CS4224', 'Discrete 2', 3.5),
(7, 'CS 125', 'Database Management Systems', 5);

-- --------------------------------------------------------

--
-- Table structure for table `qa_sub_enrolled`
--

CREATE TABLE `qa_sub_enrolled` (
  `sub_enrolled_id` int(10) NOT NULL,
  `class_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_sub_enrolled`
--

INSERT INTO `qa_sub_enrolled` (`sub_enrolled_id`, `class_id`, `user_id`) VALUES
(24, 6, 15);

-- --------------------------------------------------------

--
-- Table structure for table `qa_users`
--

CREATE TABLE `qa_users` (
  `user_id` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type_id` int(10) NOT NULL,
  `profile_id` int(10) NOT NULL,
  `user_verify` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_users`
--

INSERT INTO `qa_users` (`user_id`, `email`, `password`, `user_type_id`, `profile_id`, `user_verify`) VALUES
(2, 'jenniferetang89@gmail.com', 'jema', 3, 1, 1),
(6, 'cherylanne_tadefa@yahoo.com', 'cccccccc', 2, 5, 1),
(15, 'spabs5678@gmail.com', 'bossanne', 1, 16, 1),
(16, 'ronaldreyventula@gmail.com', 'genetalang', 1, 17, 1),
(20, 'narridon@gmail.com', 'bossshel', 2, 22, 1),
(21, 'faith@mail.jemala', '87654321', 1, 25, 1),
(24, 'jenn@mail.jemala', '123456789', 1, 28, 1),
(25, 'new@mail.com', '1234567890', 1, 29, 1),
(27, 'stephen_janz93@yahoo.com', 'suckit143', 1, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `qa_user_type`
--

CREATE TABLE `qa_user_type` (
  `user_type_id` int(10) NOT NULL,
  `user_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qa_user_type`
--

INSERT INTO `qa_user_type` (`user_type_id`, `user_type`) VALUES
(1, 'student'),
(2, 'teacher'),
(3, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `qa_add_info`
--
ALTER TABLE `qa_add_info`
  ADD PRIMARY KEY (`add_info_id`);

--
-- Indexes for table `qa_announce`
--
ALTER TABLE `qa_announce`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `qa_books`
--
ALTER TABLE `qa_books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `qa_class`
--
ALTER TABLE `qa_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `qa_courses`
--
ALTER TABLE `qa_courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `qa_forum_comment`
--
ALTER TABLE `qa_forum_comment`
  ADD PRIMARY KEY (`forum_comment_id`);

--
-- Indexes for table `qa_forum_reply`
--
ALTER TABLE `qa_forum_reply`
  ADD PRIMARY KEY (`forum_reply_id`);

--
-- Indexes for table `qa_forum_topic`
--
ALTER TABLE `qa_forum_topic`
  ADD PRIMARY KEY (`forum_topic_id`);

--
-- Indexes for table `qa_profile`
--
ALTER TABLE `qa_profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `qa_stud_ga`
--
ALTER TABLE `qa_stud_ga`
  ADD PRIMARY KEY (`stud_ga_id`);

--
-- Indexes for table `qa_subjects`
--
ALTER TABLE `qa_subjects`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `qa_sub_enrolled`
--
ALTER TABLE `qa_sub_enrolled`
  ADD PRIMARY KEY (`sub_enrolled_id`);

--
-- Indexes for table `qa_users`
--
ALTER TABLE `qa_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `qa_user_type`
--
ALTER TABLE `qa_user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `qa_add_info`
--
ALTER TABLE `qa_add_info`
  MODIFY `add_info_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `qa_announce`
--
ALTER TABLE `qa_announce`
  MODIFY `announce_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `qa_books`
--
ALTER TABLE `qa_books`
  MODIFY `book_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `qa_class`
--
ALTER TABLE `qa_class`
  MODIFY `class_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qa_courses`
--
ALTER TABLE `qa_courses`
  MODIFY `course_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qa_forum_comment`
--
ALTER TABLE `qa_forum_comment`
  MODIFY `forum_comment_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `qa_forum_reply`
--
ALTER TABLE `qa_forum_reply`
  MODIFY `forum_reply_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `qa_forum_topic`
--
ALTER TABLE `qa_forum_topic`
  MODIFY `forum_topic_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `qa_profile`
--
ALTER TABLE `qa_profile`
  MODIFY `profile_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `qa_stud_ga`
--
ALTER TABLE `qa_stud_ga`
  MODIFY `stud_ga_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `qa_subjects`
--
ALTER TABLE `qa_subjects`
  MODIFY `sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qa_sub_enrolled`
--
ALTER TABLE `qa_sub_enrolled`
  MODIFY `sub_enrolled_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `qa_users`
--
ALTER TABLE `qa_users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `qa_user_type`
--
ALTER TABLE `qa_user_type`
  MODIFY `user_type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
