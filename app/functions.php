<?php
include_once 'Session.php';
include 'db_conn.php';

Class Functions
{
	private $db;
	public function __construct(){
		$this->db = new db_conn(); 
	}

	// check by query
	public function getLoginCheck($data){
		$email    = $data['email'];
		$password = $data['password'];

		$chk_email = $this->checkEmail($email);
		$chk_uv = $this->checkUserVerify($email);

		if ($email == "" OR $password == "") {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not be Empty</div>";
			return $msg;
		}

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address is not valid!</div>";
			return $msg;
		}

		if ($chk_email == false) {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address Not Exist!</div>";
			return $msg;
		}

		if($chk_uv == 0){
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Please wait for the Admin to verify your account!</div>";
			return $msg;	
		}

		$data = $this->getLogin($email, $password);
		if ($data) {
 			Session::init();
			Session::set("login", true);
			Session::set("user_id", $data->user_id);
			Session::set("email", $data->email);
			Session::set("user_type_id", $data->user_type_id);
			Session::set("profile_id", $data->profile_id);
			if($data->user_type_id == 3){
				Session::set("loginmsg", "<div class='alert alert-success'><strong>Success ! </strong>You are successfully logged in!</div>" );
			}
			else{
				Session::set("loginmsg", "<div class='alert alert-success'><strong>Success ! </strong>You are successfully logged in!</div>");
			}
			//redirect account to...
			if($data->user_type_id==1){
				header('Location: acc/student/home');
			}
			else if($data->user_type_id==2){
				header('Location: acc/teacher/home');
			}
			else{
				header("Location: acc/admin/home");
					}

		}else{
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Wrong Password!</div>";
			return $msg;
		}
	}

	public function checkEmail($email){
		$sql = "SELECT email FROM qa_users WHERE email = :email";
		$query = $this->db->conn->prepare($sql); 
		$query->bindValue(':email', $email);
		$query->execute();
		if ($query->rowCount() > 0) {
				return true;
			}else{
				return false;
		}
	}

	public function checkPassword($password){
		$sql = "SELECT password FROM qa_users WHERE password = :pass";
		$query = $this->db->conn->prepare($sql); 
		$query->bindValue(':pass', $password);
		$query->execute();
		if ($query->rowCount() > 0) {
				return true;
			}else{
				return false;
		}
	}

	public function checkUserVerify($email){
		$sql = "SELECT * FROM qa_users WHERE email = :email AND user_verify = 1";
		$query = $this->db->conn->prepare($sql);
		$query->bindValue(':email', $email);
		$query->execute();
		$r = $query->rowCount();
		if ($r){
				return 1;
			}else{
				return 0;
		}	
	}

	//check by traps
	public function checkEmailReg($email){
		$chk_email = $this->checkEmail($email);
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
	    	Session::set("msg","<div class='alert alert-danger'><strong>Error ! </strong>Invalid email address!</div>");
	    	return 1;
	    }
	    if($chk_email == true){
	    	Session::set("msg","<div class='alert alert-warning'><strong>Warning ! </strong>Email already exist!</div>");
	    	return 1;
	    }  
	}

	public function checkSignup($data){
		//call pass & email
		$email = $data['email'];
	    $pass = $data['pass'];
		$chk_email = $this->checkEmail($email);
	    $chk_pass = $this->checkPassword($pass);
	    // check pass & email
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
	    	Session::set("msg","<div class='alert alert-danger'><strong>Error ! </strong>Invalid email address!</div>");
	    	return 1;
	    }
	    if($chk_pass == true){
	    	Session::set("msg","<div class='alert alert-warning'><strong>Warning ! </strong>Weak password.</div>");
	    	return 1;
	    }
	    if($chk_email == true){
	    	Session::set("msg","<div class='alert alert-warning'><strong>Warning ! </strong>Email already exist!</div>");
	    	return 1;
	    }
	    if($pass == $_POST['pass2']){
		    if(strlen($pass) < 8){
		    	Session::set("msg","<div class='alert alert-danger'><strong>Error ! </strong>Password should consist atleast eight characters.</div>");
		    	return 1;
		    }
		}
		else{
			Session::set("msg","<div class='alert alert-danger'><strong>Error ! </strong>Confirm password please.</div>");
			return 1;
		}

		// if all false
		return 0;
	}

	public function checkBook($book){
		$sql = "SELECT * FROM qa_books WHERE book_name=:book";
		$query = $this->db->conn->prepare($sql);
		$query->execute([':book' => $book]);
		$data = $query->fetch(PDO::FETCH_OBJ);
		return $data->book_name;
	}

	public function checkBookName($bookname){
		$sql = "SELECT book_name FROM qa_books WHERE book_name=:bname";
		$query = $this->db->conn->prepare($sql);
		$query->bindValue(':bname', $bookname); 
		$query->execute();
		$x = $query->rowCount();
		if($x > 0) {
			for($i=$x; $i==$x; $i++){
				$renamed = $bookname." (".$x.")";
				if($renamed == $this->checkBook($renamed)){
					$x++;
				}
			}
			return $renamed;
		}
		else{ 
			return $bookname;
		}
	}

	public function checkData($find, $table, $field, $fieldfind2){
		$sql = "SELECT ".$field." FROM ".$table." WHERE ".$field." = :find AND ".$fieldfind2;
		$query = $this->db->conn->prepare($sql); 
		$query->bindValue(':find', $find);
		$query->execute();
		if ($query->rowCount() > 0) {
				return 1;
			}else{
				return 0;
		}
	}

	// getting of data
	public function getLogin($email, $password){

		$sql = 'SELECT * FROM qa_users WHERE email = :email AND password = :password'; 
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute(
			[
				':email' 	=> $email,
				':password' => $password
			] 
		);
		$data = $stmt->fetch(PDO::FETCH_OBJ);
		return $data;
	}

	public function getNewData($table, $find_by, $find_val, $find_id){
		$sql = "SELECT * FROM ".$table." WHERE ".$find_by." = :find_by ORDER BY ".$find_id." DESC LIMIT 1";
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':find_by'=> $find_val]);
		$data = $stmt->fetch(PDO::FETCH_OBJ);

		return $data;
	}

	public function getAllEmail($id){
		$sql = 'SELECT * FROM qa_users WHERE user_type_id = '.$id;
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function getData($id, $table, $prefix){
		$sql = 'SELECT * FROM '.$table.' WHERE '.$prefix.'_id = :id';
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':id'=>$id]);
		$data = $stmt->fetch(PDO::FETCH_OBJ);
		return $data;
	}

	public function getData2($find, $table, $field){
		$sql = 'SELECT * FROM '.$table.' WHERE '.$field.' = :find';
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':find'=>$find]);
		$data = $stmt->fetch(PDO::FETCH_OBJ);
		return $data;
	}

	public function getData3($id, $id2, $table, $prefix, $prefix2){
		$sql = 'SELECT * FROM '.$table.' WHERE '.$prefix.'_id = :id AND '.$prefix2.'_id = :id2';
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':id'=>$id, ':id2' => $id2]);
		$data = $stmt->fetch(PDO::FETCH_OBJ);
		return $data;
	}

	public function getAllData($table){
		$sql = 'SELECT * FROM '.$table;
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function getAllData2($table, $field){
		$sql = 'SELECT * FROM '.$table.' ORDER BY '.$field.' DESC';
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function getDataReg($id, $table, $prefix, $getorder){
		$sql = 'SELECT * FROM '.$table.' WHERE '.$prefix.'_id = :id ORDER BY '.$getorder.' DESC LIMIT 1';
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':id'=>$id]);
		$data = $stmt->fetch(PDO::FETCH_OBJ);
		return $data;	
	}

	// adding of data
	public function addInfo($data){
		$course_id = $data['course'];
		$sec   = $data['section'];
		$yr    = $data['year'];
		$stat  = $data['status'];

		$sql = "INSERT INTO qa_add_info (course_id, section, year, status) VALUES (:course_id, :sec, :yr, :stat)";
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':course_id' => $course_id,
						':sec' => $sec,
						':yr' => $yr,
						':stat' => $stat]);
	
		$d = $this->getDataReg($course_id,'qa_add_info','course','add_info_id');

		return $d->add_info_id;
	}

	public function addProfile($data, $add_info_id){
		$fname = $data['fname'];
		$mname = $data['mname'];
		$lname = $data['lname'];
		if($add_info_id == 0){
			$sql = "INSERT INTO qa_profile (fname, mname, lname) VALUES (:fname, :mname, :lname)";
				$stmt = $this->db->conn->prepare($sql);
				$r = $stmt->execute([':fname' => $fname,
									  ':mname' => $mname,
									  ':lname' => $lname]);
				if ($r) { 
					$d2 = $this->getNewData('qa_profile', 'lname', $lname, 'profile_id');
					$id = $d2->profile_id;
					return $id;
				}
				else{
					$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
					return $msg;
				}
		}
		else{

			$sql = "INSERT INTO qa_profile (fname, mname, lname, add_info_id) VALUES (:fname, :mname, :lname, :add_info_id)";
				$stmt = $this->db->conn->prepare($sql);
				$r = $stmt->execute([':fname' => $fname,
									  ':mname' => $mname,
									  ':lname' => $lname,
									  ':add_info_id' => $add_info_id]);
				if ($r) {
					$d2 = $this->getDataReg($add_info_id,'qa_profile','add_info','profile_id');
					$id = $d2->profile_id;
					return $id;
				}
				else{
					$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
					return $msg;
				}
		}
	}

	public function addProfile2($email, $data, $add_info_id){
		$fname = $data['fname'];
		$mname = $data['mname'];
		$lname = $data['lname'];

		$imagefile = $_FILES['picture']['name'];
		  $dir = 'lib/';
		  $image_extension = strtolower(pathinfo($imagefile,PATHINFO_EXTENSION));
		  $valid_extensions = array('png','jpg','jpeg','gif');
		  $image = $dir.$email.".".$image_extension;

			// if(in_array($bookExtension, $valid_extensions)){
				if($_FILES['picture']['size'] < 1000000000){
					if(move_uploaded_file($_FILES['picture']['tmp_name'],$image)){
						if($add_info_id == 0){
							$sql = "INSERT INTO qa_profile (fname, mname, lname, profile_image) VALUES (:fname, :mname, :lname, :image)";
								$stmt = $this->db->conn->prepare($sql);
								$r = $stmt->execute([':fname' => $fname,
													  ':mname' => $mname,
													  ':lname' => $lname,
													  ':image' => $image]);
								if ($r) {
									$d2 = $this->getNewData('qa_profile', 'lname', $lname, 'profile_id');
									$id = $d2->profile_id;
									return $id;
								}
								else{
									$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
									return $msg;
								}
						}
						else{

							$sql = "INSERT INTO qa_profile (fname, mname, lname, profile_image,add_info_id) VALUES (:fname, :mname, :lname, :image, :add_info_id)";
								$stmt = $this->db->conn->prepare($sql);
								$r = $stmt->execute([':fname' => $fname,
													  ':mname' => $mname,
													  ':lname' => $lname,
													  ':image' => $image,
													  ':add_info_id' => $add_info_id]);
								if ($r) {
									$d2 = $this->getDataReg($add_info_id,'qa_profile','add_info','profile_id');
									$id = $d2->profile_id;
									return $id;
								}
								else{
									$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
									return $msg;
								}
						}
					}
					else{
						$errMSG = "something went wrong!";
						return $errMSG;
					}
				}
				else{
					$errMSG = "Sorry, Your Image Is Too Large To Upload. It Should Be Less Than 1GB.";
					return $errMSG;
				}
			// }
			// else{
			// 	$errMSG = "Sorry only PNG, JPG, JPEG, and GIF Extension File is Allowed.";
			// 	return $errMSG;		
			// }
	}

	public function userRegister($data, $prof_id, $user_type_id){
		$email = $data['email'];
		$pass  = $data['pass'];
		$pass2 = $data['pass2'];   
		$chk_email = $this->checkEmail($email);
		$chk_pass = $this->checkPassword($pass);
		if($pass == "" OR $pass2 == "" OR $email ==""){
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not be Empty.</div>";
			return $msg;
		}		
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address is not valid!</div>";
			return $msg;
		}
		if ($chk_email == true){
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address is already Exist!.</div>";
			return $msg;
		}
		if($chk_pass == true){
	    	$msg = "<div class='alert alert-warning mt-4'><strong>Warning ! </strong>Weak password!</div>";
	    	return $msg;	
	    }
		if($pass == $pass2){
			if(strlen($pass) < 8){
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The password should be atleast eight characters.</div>";
			return $msg;
			}
		}
		else{
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Confirm Your Password.</div>";
			return $msg;
		}
		
			$sql = "INSERT INTO qa_users (email,password,user_type_id,profile_id) VALUES (:email,:pass,:ut_id,:prof_id)";
			$stmt = $this->db->conn->prepare($sql);
			$r = $stmt->execute([':email' => $email,
								  ':pass' => $pass,
								  ':ut_id' => $user_type_id,
								  ':prof_id' => $prof_id]);
			if ($r) {
				return 1;
			}
			else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
				return $msg;
			}
	}

	public function addClass($data, $user_id){

		$sql = "INSERT INTO qa_class (user_id, sub_id, course_id, year, section, class_time, class_day, class_room) VALUES (:user_id, :sub_id, :course_id, :year, :section, :class_time, :class_day, :class_room)";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':user_id' => $user_id,
							 ':sub_id' => $data['subject'],
							 ':course_id' => $data['course'],
							 ':year' => $data['year'],
							 ':section' => $data['section'],
							 ':class_time' => $data['time'],
							 ':class_day' => $data['day'],
							 ':class_room' => $data['room']]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function addSubEnrolled($class_id, $user_id){
		$sql = "INSERT INTO qa_sub_enrolled (class_id, user_id)VALUES(:c_id, :u_id);";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':u_id' => $user_id, ':c_id' => $class_id]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function addStudGA($sub_id, $user_id, $sub_enrolled_id){
		$sql = "INSERT INTO qa_stud_ga (sub_enrolled_id, user_id, sub_id) VALUES (:se_id, :u_id, :s_id)";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':se_id' => $sub_enrolled_id,
							 ':u_id' => $user_id,
							 ':s_id' => $sub_id]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function addBook($data){
		//book info
		$name = $this->checkBookName($_POST['bname']);
		$category = $_POST['bcategory'];
		$author = $_POST['bauthor'];
		$description = $_POST['bdescription'];
		$permit = $_POST['bpermit'];
		
		$bookFile = $_FILES['book']['name'];
			$dir = '../../library/';
			$bookExtension = strtolower(pathinfo($bookFile,PATHINFO_EXTENSION));
			$valid_extensions = array('pdf');
			// basename($bookFile)
			$book = $dir.$name.".".$bookExtension;
			if(in_array($bookExtension, $valid_extensions)){
				if($_FILES['book']['size'] < 1000000000){
					if(move_uploaded_file($_FILES['book']['tmp_name'],$book)){
					$sql = "INSERT INTO qa_books (sub_id, book_name, book_author, book_description, book_file, book_permit) VALUES (:category, :name, :author, :description, :book, :permit)";
					$stmt = $this->db->conn->prepare($sql);
					$r = $stmt->execute([':category' => $category,
										 ':name' => $name,
										 ':author' => $author,
										 ':description' => $description,
										 ':book' => $book,
										 ':permit' => $permit]);
					$d = $this->getNewData('qa_books','book_name', $name, 'book_id');
					return $d->book_id;
					}
					else{
						$errMSG = "something went wrong!";
						return $errMSG;
					}
				}
				else{
					$errMSG = "Sorry, Your File Is Too Large To Upload. It Should Be Less Than 1GB.";
					return $errMSG;
				}
			}
			else{
				$errMSG = "Sorry only PDF Extension File is Allowed.";
				return $errMSG;		
			}	
	}

	public function addSubject($data){

		$sql = "INSERT INTO qa_subjects (sub_code, sub_name, sub_unit) VALUES (:scode, :sname, :unit)";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':scode' => $data['code'],
							 ':sname' => $data['sname'], 
							 ':unit' => $data['unit']]);

		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function addCourse($data){

		$sql = "INSERT INTO qa_courses (course_name, major) VALUES (:cname, :major)";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':cname' => $data['cname'],
							 ':major' => $data['major']]);

		if($r){
			return 1;
		}
		else{
			return 0;
		}	
	}

	public function addForumTopic($user, $data){
		$now = $this->timeNow();
		$sql = "INSERT INTO qa_forum_topic (user_id, topic, topic_date)VALUES(:user_id, :topic, :topic_date);";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':user_id' => $user,
							 ':topic' => $data['topic'],
							 ':topic_date' => $now]);
		if($r){
			return 1;
		}else{
			return 0;
		}
	}

	public function addAnnounce($data){
		$now = $this->timeNow();
		$sql = "INSERT INTO qa_announce (announce, announce_date)VALUES(:announce, :announce_date);";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':announce' => $data['announce'],
							 ':announce_date' => $now]);
		if($r){
			return 1;
		}else{
			return 0;
		}
	}

	public function addForumComment($user, $data, $topic_id){
		$now = $this->timeNow();
		$sql = "INSERT INTO qa_forum_comment (forum_topic_id, user_id, comment, comment_date)VALUES(:topic_id, :user_id, :comment, :comment_date);";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':topic_id' => $topic_id,
							 ':user_id' => $user,
							 ':comment' => $data['comment'],
							 ':comment_date' => $now]);
		if($r){
			return 1;
		}else{
			return 0;
		}
	}

	public function addForumReply($user, $data, $comment_id){
		$now = $this->timeNow();
		$sql = "INSERT INTO qa_forum_reply (forum_comment_id, user_id, reply, reply_date)VALUES(:comment_id, :user_id, :reply, :reply_date);";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':comment_id' => $comment_id,
							 ':user_id' => $user,
							 ':reply' => $data['reply'],
							 ':reply_date' => $now]);
		if($r){
			return 1;
		}else{
			return 0;
		}
	}


	//updating of data
	public function editPass($id, $pass){
		$sql = "UPDATE qa_users SET password=:pass WHERE user_id = :id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':pass' => $pass, ':id' => $id]);
		if($r){
			return 1;
		}else{
			return 0;
		}
	}
	
	public function editProfile($id, $data, $option){
		$fname = $data['fname'];
		$mname = $data['mname'];
		$lname = $data['lname'];

		if($option == 1){
			$sql = "UPDATE qa_profile SET fname=:fname, mname=:mname, lname=:lname WHERE profile_id=:id";
			$stmt = $this->db->conn->prepare($sql);
			$r = $stmt->execute([':fname' => $fname,
								 ':mname' => $mname,
								 ':lname' => $lname,
								 ':id' => $id]);
		}
			if ($r) {
				
				return $id;
			}
			else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been a problem updating your details.</div>";
				return $msg;
			}
	}

	public function editInfo($data, $id){
		$course_id = $data['course'];
		$sec   = $data['section'];
		$yr    = $data['year'];
		$stat  = $data['status'];

		$sql = "UPDATE qa_add_info SET course_id=:course_id, section=:sec, year=:yr, status=:stat WHERE add_info_id =:id";
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute([':id' => $id,
						':course_id' => $course_id,
						':sec' => $sec,
						':yr' => $yr,
						':stat' => $stat]);
	
		$d = $this->getDataReg($course_id,'qa_add_info','course','add_info_id');

		return $d->add_info_id;
	}

	public function editCourse($data, $id){
		$sql = "UPDATE qa_courses SET course_name=:cname, major=:major WHERE course_id=:c_id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':cname' => $data['cname'],
							 ':major' => $data['major'],
							 ':c_id' => $id]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function editClass($data, $id){
		$sql = "UPDATE qa_class SET sub_id=:sub_id, course_id=:course_id, year=:year, section=:section, class_time=:class_time, class_day=:class_day, class_room=:class_room WHERE class_id=:class_id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':class_id' => $id,
							 ':sub_id' => $data['subject'],
							 ':course_id' => $data['course'],
							 ':year' => $data['year'],
							 ':section' => $data['section'],
							 ':class_time' => $data['time'],
							 ':class_day' => $data['day'],
							 ':class_room' => $data['room']]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function editSubject($data, $id){
		$sql = "UPDATE qa_subjects SET sub_code=:sub_code, sub_name=:sub_name, sub_unit=:sub_unit WHERE sub_id=:sub_id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':sub_id' => $id,
							 ':sub_code' => $data['code'],
							 ':sub_name' => $data['sname'],
							 ':sub_unit' => $data['unit']]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}

	//////////////on progress here
	public function editGrade($data, $id){
		$sql = "UPDATE qa_stud_ga SET sub_grade=:grade WHERE stud_ga_id=:id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':id' => $id,
							 ':grade' => $data['student_grade']]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}


	public function editBook2($data, $id){

		$bookFile = $_FILES['book']['name'];
			$dir = '../../library/';
			$bookExtension = strtolower(pathinfo($bookFile,PATHINFO_EXTENSION));
			$valid_extensions = array('pdf');
			// basename($bookFile)
			$book = $dir.$name.".".$bookExtension;
			if(in_array($bookExtension, $valid_extensions)){
				if($_FILES['book']['size'] < 1000000000){
					if(move_uploaded_file($_FILES['book']['tmp_name'],$book)){
						$sql = "UPDATE qa_books SET sub_id=:category, book_name=:name, book_author=:author, book_file=:book, book_description=:description, book_permit=:permit WHERE book_id=:book_id";
						$stmt = $this->db->conn->prepare($sql);
						$r = $stmt->execute([':book_id' => $id,
											 ':category' => $data['bcategory'],
											 ':name' => $data['bname'],
											 ':author' => $data['bauthor'],
											 ':book' => $book,
											 ':description' => $data['bdescription'],
											 ':permit' => $data['bpermit']]);
						return 1;
					}
					else{
						return 0;
					}
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}
	}

	public function editBook($data, $id){

		$sql = "UPDATE qa_books SET sub_id=:category, book_name=:name, book_author=:author, book_description=:description, book_permit=:permit WHERE book_id=:book_id";
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute([':book_id' => $id,
							 ':category' => $data['bcategory'],
							 ':name' => $data['bname'],
							 ':author' => $data['bauthor'],
							 ':description' => $data['bdescription'],
							 ':permit' => $data['bpermit']]);
		if($r){
			return 1;
		}
		else{
			return 0;
		}
	}


	public function editRequest($id){
		$sql = "UPDATE qa_users SET user_verify = 1 WHERE user_id =".$id;
		$stmt = $this->db->conn->prepare($sql);
		$r = $stmt->execute();
		if($r){
			return 1;
		}
		else{
			return 0;
		}	
	}

	// deleting of data
	public function delete($table, $prefix, $id){
		$sql = 'DELETE FROM '.$table.' WHERE '.$prefix.'_id = '.$id;
		$stmt = $this->db->conn->prepare($sql);
		$stmt->execute();
		return 1;
	}


	//navigating of admin's users
	public function goUsers($utype){
		if($utype==3){
    		$go = 'users';
		}
		else if($utype==2){
			$go = 'teachers';
      	}
      	else{
      		$go = 'students';
      	}
      	return $go;
	}

	public function goUsersProfile($utype){
		if($utype==1){
    		$go = 'users-profile2';
    	}
      	else{
      		$go = 'users-profile';
      	}
      	return $go;
	}

	//encryption && decription
	public function e($sData){
		$id=(double)$sData*525325.24;
		return base64_encode($id);
	}
	public function d($sData){
		$url_id=base64_decode($sData);
		$id=(double)$url_id/525325.24;
		return $id;
	}

	// time interval
	public function timeCompute($time){
		// date_default_timezone_set('Etc/GMT-8');
		// $now = date('Y-m-d H:i:s');
		$now = $this->timeNow();
		$asnow = date_create_from_format('Y-m-d H:i:s', $now);
		$timed = date_create_from_format('Y-m-d H:i:s', $time);
		$count = date_diff($asnow,$timed);

		//seconds ago
		if($count->format("%s")<60 && $count->format("%i")<1 && $count->format("%d")<1){
			if($count->format("%s")<6){
				return $count->format("Just now.");
			}else{
				return $count->format("%s seconds ago.");
			}
		}
		//minutes ago
		if($count->format("%i")<60 && $count->format("%h")<1){
			if($count->format("%i")==1){
				return $count->format("1 minute ago.");
			}else{	
				return $count->format("%i minutes ago.");
			}
		}
		//hours ago
		if($count->format("%h")<23 && $count->format("%d")<1){
			if($count->format("%h")==1){
				return $count->format("1 hour ago.");
			}else{
				return $count->format("%h hours ago.");
			}
		}
		//days ago
		if($count->format("%d")<3){
			if($count->format("%d")==1){
				return $count->format("%d day ago.");
			}else{
				return $count->format("%d days ago.");
			}
		}else{
			return $timed->format("M d, Y - h:ia.");
		}	
	}

	public function timeNow(){
		date_default_timezone_set('Etc/GMT-8');
		$now = date('Y-m-d H:i:s');
		return $now;
	}
}

?>