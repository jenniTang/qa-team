<?php
    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../../Session.php';
    Session::init();
    include ('../../functions.php');
	$function = new Functions();



	//for the PROFILE edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_user'])){
	  	$profile_id = $_GET['p_id'];
	  	$usrRegi = $function->editProfile($profile_id,$_POST,1);
		  	if(isset($usrRegi)){
		  		if($usrRegi > 0){
		  			Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Profile is updated!</div>");
		  		} else{
		      		Session::set("msg", $usrRegi);
		      	}
		    }
		      	header("Location: profile#SuccessUPDATE!");
	}

	// for the CHANGE PASS
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['change_pass'])){
		$user_id = $_GET['u_id'];
		$pass = $_POST['pass'];
		if($pass==$_POST['pass2']){
			$checkPass = $function->checkPassword($pass);
			if(strlen($pass) < 8){
		    	Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Password should consist atleast eight characters.</div>");
		    	header("Location: change-pass?u_id=");
		    }
		    else{
				if($checkPass==false){
				$flag = $function->editPass($user_id, $pass);
					if($flag==1){
						header("Location: change-pass?cp=143#SuccessChangePassword");
					}else{
						Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
						header("Location: change-pass");
					}
				}
				else{
					Session::set("msg3","<div class='alert alert-warning mt-4'><strong>Error ! </strong>Weak Password.</div>");
					header("Location: change-pass");
				}
			}
		}
		else{
			Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Confirm password please.</div>");
			header("Location: change-pass");
		}
	}


	//for the CLASS add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_class'])){
		$user_id = $_GET['u_id'];
		$flag = $function->addClass($_POST, $user_id);
			if($flag == 1){
				Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Class has been added!</div>");
			}
			else{
				Session::set("msg2","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
		
		header("Location: classes");
	}

	//for the CLASS edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_class'])){
		$class_id = $_GET['c_id'];
		$flag = $function->editClass($_POST, $class_id);
			if($flag == 1){
				Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>Class has been updated!</div>");
			}
			else{
				Session::set("msg2","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
		
		header("Location: classes");
	}

	// for adding student on teacher's record
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_student'])){
		$class_id = $_GET['c_id'];
		$student = $function->getData2($_POST['student_email'], 'qa_users', 'email');
		$stud_id = $student->user_id;

		$checkStud = $function->checkData($stud_id, 'qa_sub_enrolled', 'user_id', 'class_id ='.$class_id);
		
		if($checkStud==0){
			if($student->user_type_id==1){
				
				$flag = $function->addSubEnrolled($class_id, $stud_id);
					if($flag == 1){
						$class = $function->getData($class_id, 'qa_class', 'class');
						$enrolled = $function->getDataReg($class_id, 'qa_sub_enrolled', 'class', 'sub_enrolled_id');
						$flag2 = $function->addStudGA($class->sub_id, $stud_id, $enrolled->sub_enrolled_id);
						if($flag2 == 1){
							Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Student has been added!</div>");
						}
						else{
							Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Grading Data.</div>");
						}
					}
					else{
						Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Subject Enrolled.</div>");
					}	
			}
			else{
				Session::set("msg","<div class='alert alert-warning mt-4'><strong>Warning ! </strong> You are trying to add a none student.</div>");
			}
		}
		else{
			Session::set("msg","<div class='alert alert-warning mt-4'><strong>Warning ! </strong> The student already exist.</div>");
		}
		header("Location: class-record");
	}

	//for the Grade edit Teacher
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_grade'])){
		$stud_ga_id = $_GET['sg_id'];
		$class_id = $_GET['c_id'];;
		$stud_ga_id_e = $function->e($stud_ga_id);

		$flag = $function->editGrade($_POST, $stud_ga_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Student's grade has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
			header("Location: class-record?c_id=".$class_id);
	}


	//for the FORUM post
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_topic'])){
		if($_POST['topic']==""){
			$user_id = Session::get("user_id");
			$flag = $function->addForumTopic($user_id, $_POST);
			if($flag>0){
				Session::set("msg","<div class='alert alert-default mt-4'><i>Your topic has been posted</i></div>");
			}
			else{
				Session::set("msg","<div class='alert alert-default mt-4'><strong>Unable to post! </strong>Try again.</div>");
			}
		}else{
			Session::set("msg","<div class='alert alert-default mt-4'><strong>Your post is empty! </strong>Try again.</div>");
		}

		header("Location: forum");
	}

	//for the FORUM comment
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_comment'])){
		$user_id = Session::get("user_id");
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumComment($user_id, $_POST, $forum_topic_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Commented..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum#post".$function->e($forum_topic_id));
	}

	//for the FORUM reply
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_reply'])){
		$user_id = Session::get("user_id");
		$forum_comment_id = $_GET['fc_id'];
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumReply($user_id, $_POST, $forum_comment_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Replied..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum#post".$function->e($forum_topic_id));
	}


	//Deletion
	if(isset($_GET['delete'])){
		$del = $_GET['delete'];

		//[1] Class delete
		if($del==1){
			$class_id = $_GET['c_id'];
			$class = $function->getAllData('qa_sub_enrolled');
			foreach ($class as $val){
				if($val['class_id']==$class_id){
					$function->delete('qa_stud_ga', 'sub_enrolled', $val['sub_enrolled_id']);
					$function->delete('qa_sub_enrolled', 'sub_enrolled', $val['sub_enrolled_id']);
				}	
			}
			$function->delete('qa_class', 'class', $class_id);

			Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A class has been deleted!</div>");
			header("Location: classes");
		}

		//[2] Student Delete
		if($del==2){
			$stud_ga_id = $function->d($_GET['sg_id']);
			$studGA = $function->getData($stud_ga_id, 'qa_stud_ga', 'stud_ga');
			$function->delete('qa_sub_enrolled', 'sub_enrolled', $studGA->sub_enrolled_id);
			$function->delete('qa_stud_ga', 'stud_ga', $stud_ga_id);

			Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A student has been removed!</div>");
			header("Location: class-record?c_id=".$_GET['c_id']);
		}

	}

?>