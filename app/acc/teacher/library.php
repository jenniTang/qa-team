<?php include ('header.php');?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>



<section class="content">
  
<div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
	<p><h3 class="text-center col-md-12"><b>ICT BOOKS</b></h3></p>

	<!-- Book data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th></th>
								<th>CATEGORY</th>
								<th>BOOK NAME</th>
								<th>AUTHOR</th>
								<!-- <th>DESCRIPTION</th>
								<th>PERMIT</th> -->
							</tr>
						</thead>
						<tbody class="datashow">
							<?php
								$data = $function->getAllData('qa_books');
								$i = 0;
								foreach($data as $val):
									$book_id = $val['book_id'];
									$data2 = $function->getData($val['sub_id'],'qa_subjects','sub');

									if($book_id){ $i += 1; ?>
							<tr><td><a href="library?b_id=<?=$book_id;?>#viewdetails"><?=$i;}?></a></td>
								<td><a class="pull-left" href="library?b_id=<?=$book_id;?>#viewdetails"><?=$data2->sub_name;?></a></td>
								<!--  target="_blank" -->
								<td><a class="pull-left" href="library?b_id=<?=$book_id;?>#viewdetails"><?=$val['book_name'];?></a></td>
								<td><a class="pull-left" href="library?b_id=<?=$book_id;?>#viewdetails"><?=$val['book_author'];?></a></td>
								<!-- <td><a href="#"><?=$val['book_description'];?></a></td>
								<td><a href="#"><?=$val['book_permit'];?></a></td> -->
							</tr>
							<?php endforeach;?>
						</tbody>
						<tfoot><tr><th></th></tr></tfoot>
					</table>
					<br>
    			</form>
    		</div>
    	</div>
    </div>


    <?php if(isset($_GET['b_id'])){
	    $book_id = $_GET['b_id'];
		$data = $function->getData($book_id,'qa_books','book');
		$PDFfilename = $data->book_file;
    ?>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<p id="viewdetails"></p>
    	<div class="col-lg-12 panel info-body-md">
    		<div class="panel-body">
		    	<p class="mt-4">View book:</p>
		    	
		    		<a class="btn btn-default form-control" target="_blank" href="review?bopen=1&file=<?=$function->e($data->book_id);?>"><?=$data->book_name;?></a>
		    	
		    		<!-- scripts: ../../main/footer.php -->
		    		<!-- <a onclick="openPdf('<?=$PDFfilename;?>', '<?=$data->book_name;?>')" href="#" class="btn btn-default col-md-12"><?=$data->book_name.".pdf";?></a> -->
		    		<br><br>
	    	</div>
    	</div>
		<div class="comment-frame col-lg-12 ">
    		<br> 
    		<p class="text-center"><label style="font-size: 20px; color: grey;"> Book Details </label></p>	
				<h6>Book Category:<br>
  				<?php
  					$subjects = $function->getData($data->sub_id, 'qa_subjects', 'sub'); ?>
  					<b class="margin-1"><?=$subjects->sub_name ;?></b></h6>
				<h6>Book Name:<br><b class="margin-1"><?=$data->book_name;?></b></h6>
  				<h6>Author:<br><b class="margin-1"><?=$data->book_author;?></b></h6>
  				<h6>Book descriptions:<br><b class="margin-1"><?=$data->book_description;?></b></h6>
  				<h6>Permit<br><b class="margin-1"><?=$data->book_permit;?></b></h6>
    		<br>
    	</div>
	</div>

    <?php } ?>

</div>

</section>
<?php include ('../../main/footer.php'); ?>