<?php
	include ('header.php');
?>
<script src="../../css/datatable/config/config_dttbl_2.js"></script>


<section class="content">

  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">

  	<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12"></div>
  	
    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
    	
    	<!-- POSTING -->
    	<div class="col-lg-12 panel info-body-md">
    		<div class="panel-body mt-3 text-center">
    			<div class="text-left">
					<label style="font-size: 20px; color: grey;"> Post a Topic: </label>
				</div>
				<form action="navigate" method="post">
					<textarea id="topic" class="form-control" style="height: 90px;" type="text" name="topic" placeholder="Type your topic here." required=""></textarea>
					<script>
						CKEDITOR.replace( 'topic' );
					</script>
					<br>
					<div class="pull-right">
						<input type="submit" class="btn btn-primary" name="add_topic" value="POST">
					</div>
					<br><br>
				</form>
			</div>
    	</div>
    	<?php
			$msg = Session::get("msg");
			if(isset($msg)){
				echo $msg;
				Session::set("msg", NULL);
			}
		?>

	<!-- TOPICS -->
	<?php 
    	$data = $function->getAllData2('qa_forum_topic', 'forum_topic_id');
    	foreach($data as $topics):
	    	$user = $function->getData($topics['user_id'], 'qa_users', 'user');
	    	$type = $function->getData($user->user_type_id, 'qa_user_type', 'user_type');
	    	$profile = $function->getData($user->profile_id, 'qa_profile', 'profile');
	    	$timed = $function->timeCompute($topics['topic_date']);
	    	$forum_topic_id = $topics['forum_topic_id'];
	?>
    	<div class="col-lg-12 panel info-body-md" id="post<?=$function->e($forum_topic_id);?>">
    		<div class="panel-body mt-3">
    			<div class="text-left">
					<p style="color: grey;"><strong>
						<label style="font-size: 20px; color: #000;">
							<?=$profile->fname." ".$profile->lname." ";?></label></strong>
							 (<?=$type->user_type;?>) posted <?=$timed;?> <i onclick="myAccFunc('option<?=$function->e($forum_topic_id);?>')" class="fa fa-cog pull-right" style="margin-top: 0.5em; margin-right: 0.5em;"></i>

					<!-- Post EDIT/DELETE -->
					<!-- <div id="option<?=$function->e($forum_topic_id);?>" class="padd w3-display-topright w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
						<p>Choose options.</p>
					 	<a class="btn btn-success" href="#">Edit</a>
					 	<a class="btn btn-danger" href="" onclick="myAccFunc('delete<?=$function->e($forum_topic_id);?>')">Delete</a>
					 	<a class="btn btn-default" href="" onclick="myAccFunc('option<?=$function->e($forum_topic_id);?>')">Cancel</a>
					</div>
					<div id="delete<?=$function->e($forum_topic_id);?>" class="padd w3-display-topright w3-dropdown-content w3-card-4 w3-hide w3-grey text-center">
						<a class="btn btn-danger" href="#">Delete</a>
					 	<a class="btn btn-default" href="" onclick="myAccFunc('delete<?=$function->e($forum_topic_id);?>')">Cancel</a>
					</div>
					</p> -->

				</div>
				<div class="post-frame">
					<p class="margin-1"><?=$topics['topic'];?></p>

				</div>
				


				<!-- no. of Comments and Replies -->
				<?php
					$data2 = $function->getAllData('qa_forum_comment');
					$data3 = $function->getAllData('qa_forum_reply');
					$i = 0;
					$j = 0;
					foreach($data2 as $comments):
						if($comments['forum_topic_id']==$forum_topic_id){
							$i+=1;
							foreach($data3 as $replies):
								if($replies['forum_comment_id']==$comments['forum_comment_id']){
									$j+=1;
								}
							endforeach;
						}
					endforeach;
				?>
				<div class="pull-left">
					<p style="color: grey;"><?=($i==1)?$i.' comment':$i.' comments';?> | <?=($j==1)?$j.' reply':$j.' replies';?></p>
				</div><br>


				<!-- COMMENTS -->
				<?php
					foreach($data2 as $comments):
						if($comments['forum_topic_id']==$forum_topic_id){
							$user2 = $function->getData($comments['user_id'], 'qa_users', 'user');
					    	$type2 = $function->getData($user2->user_type_id, 'qa_user_type', 'user_type');
					    	$profile2 = $function->getData($user2->profile_id, 'qa_profile', 'profile');
					    	$timed2 = $function->timeCompute($comments['comment_date']);
					    	$forum_comment_id = $comments['forum_comment_id'];
				?>
						<div class='margin-2 comment-frame' id="comment<?=$function->e($forum_comment_id);?>">
							<p style="color: grey; font-size: 13px;" class="frame-fix">
								<strong><label class="margin-1" style="font-size: 14px; color: #000;">
									<?=$profile2->fname." ".$profile2->lname." ";?></label></strong>
									 (<?=$type2->user_type;?>) commented <?=$timed2;?> <i class="fa fa-cog pull-right" style="margin-top: 0.5em; margin-right: 0.5em;"></i></p>
							<p class="margin-2" style="font-size: 13px;"><?=$comments['comment'];?></p>
						
						<!-- REPLIES -->
						<?php
							foreach($data3 as $replies):
								if($replies['forum_comment_id']==$forum_comment_id){
									$user3 = $function->getData($replies['user_id'], 'qa_users', 'user');
							    	$type3 = $function->getData($user3->user_type_id, 'qa_user_type', 'user_type');
							    	$profile3 = $function->getData($user3->profile_id, 'qa_profile', 'profile');
							    	$timed3 = $function->timeCompute($replies['reply_date']);
						?>
							<div class='margin-2 margin-b comment-frame'>
								<p style="color: grey; font-size: 13px;" class="frame-fix">
									<strong><label class="margin-1" style="font-size: 14px; color: #000;">
										<?=$profile3->fname." ".$profile3->lname." ";?></label></strong>
										 (<?=$type3->user_type;?>) replied <?=$timed3;?> <i class="fa fa-cog pull-right" style="margin-top: 0.5em; margin-right: 0.5em;"></i></p>
								<p class="margin-2" style="font-size: 13px;"><?=$replies['reply'];?></p>
							</div>
					<?php
							}
						endforeach;
					?>	
						<div class="pull-right margin-b">
							<i><a onclick="myAccFunc2('replying<?=$forum_comment_id;?>')" href="#post<?=$function->e($forum_topic_id);?>">reply</a></i>
						</div>
							<br>
							<div class="col-lg-12 w3-hide" id="replying<?=$forum_comment_id;?>">
							<!-- REPLYING -->
								<form action="navigate?fc_id=<?=$forum_comment_id;?>&ft_id=<?=$forum_topic_id;?>" method="post">
									<div class="row offset-lg-1 offset-md-0 offset-sm-1">
										<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
											<div class="row">
											<textarea style="height: 35px;" type="text" class="form-control col-md-12" name="reply" required=""></textarea>
											</div>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
											<div class="row">
												<input type="submit" name="add_reply" class="btn btn-default" value="REPLY">
											</div>
										</div>
									</div>
								</form>
								<br>
							</div>


						</div>
				<?php
						}
					endforeach;
	   
	    			$msg2 = Session::get("msg<?=$forum_topic_id;?>");
	    			if(isset($msg2)){
	    				echo $msg2;
	    				Session::set("msg<?=$forum_topic_id;?>", NULL);
	    			}
	    		?>
	    		<br>
				<div class="col-lg-12">
				<!-- COMMENTING -->
					<form action="navigate?ft_id=<?=$forum_topic_id;?>" method="post">
						<div class="row offset-lg-1 offset-md-0 offset-sm-1">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
								<div class="row">
								<textarea style="height: 35px;" type="text" class="form-control col-md-12" name="comment" required=""></textarea>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<div class="row">
									<input type="submit" name="add_comment" class="btn btn-default" value="COMMENT">
								</div>
							</div>
						</div>
					</form>
				</div>
				<br>
			</div>
		</div>
	<?php endforeach; ?>

    </div>


  <!-- UPDATES by Topic -->
	<!-- <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 panel info-body-md">
	 		<div class="panel-body mt-3">
	 			<h3 class="text-center">Topics</h3>
	 				<?php foreach($data as $topics2):
	 					$user2 = $function->getData($topics2['user_id'], 'qa_users', 'user');
	 					$profile2 = $function->getData($user2->profile_id, 'qa_profile', 'profile');
	 					$timed2 = $function->timeCompute($topics2['topic_date']);
	 				?>
	 				<p class="mt-1 comment-frame">
	 					<a class="margin-1 justify" href="#"><?=$topics2['topic'];?></a><br>
	 				-<strong style="font-size: 10px"><?=$profile->fname." ".$profile->lname." ";?></strong>
	 				<i style="font-size: 9px">posted <?=$timed2;?></i>
	 				</p>
	 				<?php endforeach; ?>
			</div>
	   	</div>  	
	</div> -->
  	
  </div>
</section>


<?php include ('../../main/footer.php'); ?>