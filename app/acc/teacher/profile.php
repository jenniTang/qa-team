<?php
include ('header.php');
?>

<section class="content">

  	<div class="row offset-lg-4 offset-md-2 offset-sm-2 offset-xs-1">
  		<!-- User Profile -->
  	    <div class="col-lg-6 col-md-9 col-sm-9 col-xs-8">
	    	<div class="col-lg-12 panel info-body-md">
	    		<?php
	    			$msg = Session::get("msg");
	    			if(isset($msg)){
	    				echo $msg;
	    				Session::set("msg", NULL);
	    			}
	    		?>
		    	<div class="panel-body mt-4">
		    		<div class="text-center">
						<label style="font-size: 20px; color: grey;"> User Profile </label>
					</div>
		    		<form action="navigate?p_id=<?=$pro_id;?>" method="post">
						<h6>First Name:</h6>
						<input type="text" class="form-control" name="fname" value="<?= $userData->fname;?>"><br />
						<h6>Middle Name:</h6>
						<input type="text" class="form-control" name="mname" value="<?= $userData->mname;?>"><br />
						<h6>Last Name:</h6>
						<input type="text" class="form-control" name="lname" value="<?= $userData->lname;?>">
						<br />
						<a class="form-control btn btn-default" href="change-pass">CHANGE PASSWORD?</a>
						<br /><br />
 						<div class="pull-right">
							<!-- <a class="btn btn-danger" href="navigate?delete=1&p_id=<?=$profile_id;?>&u_id=<?=$user_id;?>&utype=<?=$utype;?>">DELETE</a> -->
							<input class="btn btn-success" type="submit" name="update_user" value="UPDATE">
						</div>
						<br /><br />
					</form>
		    	</div>
		    </div>
		</div>
  	</div>
</section>

<?php include ('../../main/footer.php'); ?>