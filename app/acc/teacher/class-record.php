<?php 
include ('header.php');
	$class_id = $function->d($_GET['c_id']);
	?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>

	
<section class="content">
  <div class="row offset-lg-0 offset-md-0 offset-xs-0">

  	<p><h4 class="text-center col-md-12"><b>CLASS STUDENTS</b></h4></p>

	<!-- Class data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th></th>
								<th>STUDENTS</th>
								<th>STATUS</th>
								<th>GRADE</th>
								<th></th>
							</tr>
						</thead>
						<tbody class="datashow">
							<?php
								$data = $function->getAllData('qa_sub_enrolled');
								$i = 0;
								foreach($data as $val):
									if($val['class_id']==$class_id){
										$i+=1;
										$stud_id = $val['user_id'];
										$student = $function->getData($stud_id, 'qa_users', 'user');
										$profile = $function->getData($student->profile_id, 'qa_profile', 'profile');
										$info = $function->getData($profile->add_info_id, 'qa_add_info', 'add_info');
										$studGA = $function->getData($stud_id, 'qa_stud_ga', 'user');
							?>
							<tr><td><a href="#"><?=$i;?></a></td>
								<td><a class="pull-left" href="#"><?=$profile->lname.", ".$profile->fname." ";?><?=($profile->mname!="")?$profile->mname[0].".":"";?></a></td>
								<td><a href="#"><?=$info->status;?></a></td>
								<td><a href="#"><?=$studGA->sub_grade;?></a></td>
								<td>
									<!-- Edit Ico -->
									<a class="frame-space" href="class-record?sg_id=<?=$function->e($studGA->stud_ga_id);?>&c_id=<?=$function->e($class_id);?>#viewdetails"><i class="edit-ico fa fa-pencil"></i></a>
									<!-- Del Ico -->
									<a class="frame-space" href='#' onclick="myAccFunc('deletion<?=$function->e($stud_id);?>')"><i class='trash-ico fa fa-trash'></i></a></td>

								<!-- Deletion Permission -->
									<div id="deletion<?=$function->e($stud_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to remove <b><?=$profile->lname.", ".$profile->fname." ";?><?=($profile->mname!="")?$profile->mname[0].".":"";?></b> in this class?</p>
									 	<a class="btn btn-danger" href="navigate?delete=2&sg_id=<?=$function->e($studGA->stud_ga_id);?>&c_id=<?=$function->e($class_id);?>">Delete</a>
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->e($stud_id);?>')">Cancel</a>
									</div>
							</tr>
							<?php } endforeach; ?> 
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table><br>
				</form>
			</div>
		</div>
	</div>

	
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<!-- to jump in -->
		<p id="viewdetails"></p>
		<p id="addnew"></p>

		<?php
			$class = $function->getData($class_id, 'qa_class', 'class');
			$class_subject = $function->getData($class->sub_id, 'qa_subjects', 'sub');
			$class_course = $function->getData($class->course_id, 'qa_courses', 'course');

		if(isset($_GET['sg_id'])){
			$stud_ga_id = $function->d($_GET['sg_id']);
			$studGA2 = $function->getData($stud_ga_id, 'qa_stud_ga', 'stud_ga');
			$stud_id2 = $studGA2->user_id;
			$student2 = $function->getData($stud_id2, 'qa_users', 'user');
			$profile2 = $function->getData($student2->profile_id, 'qa_profile', 'profile');
		?>
	<!-- Edit student grade || remove student-->
		<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4">
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> Grading of Student </label>
				</div>
				<form action="navigate?c_id=<?=$function->e($class_id);?>&sg_id=<?=$stud_ga_id;?>" method="post">
					<div class="mt-1">
						<h6>Student Name:</h6>
						<input class="form-control" type="text" name="student" value="<?=$profile2->fname." ".$profile2->lname;?>" readonly="">
					</div>
					<div class="mt-1">
						<h6>Grade:</h6>
						<input class="form-control" type="text" name="student_grade" placeholder="   grade" required="" value="<?=$studGA2->sub_grade;?>">
					</div>
					<div class="pull-right mt-3">
						<input class="btn btn-success" type="submit" name="update_grade" value="UPDATE">
					</div>
					<br><br><br>
				</form>
			</div>
		</div>

	<!-- back btn to add student -->
		<div class="col-lg-12 panel info-body-md">
			<div class="mt-4">
				<a class="btn btn-default form-control" href="class-record?c_id=<?=$class_id;?>&p_id=<?=$profile_id_e;?>&u_id=<?=$user_id;?>#addnew">Add student?</a>
			</div>
			<br>
		</div>

		<?php }else{ ?>
	<!-- Class Details -->
		<div class="comment-frame col-lg-12">
			<h6>Subject: <br><strong class="margin-1"><?=$class_subject->sub_name;?></strong></h6>
			<h6>Class: <br><strong class="margin-1"><?=$class_course->course_name;?></strong>
				<?=($class_course->major=="")?"":"<br><strong class='margin-1'>major in ".$class_course->major."</strong>";?>
				<br><strong class="margin-1"><?=$class->year.' section '.$class->section;?></strong></h6>
				<h6>Schedule: <br><strong class="margin-1">Every <?= $class->class_day;?></strong>
				<br><strong class="margin-1"><?='at '.$class->class_time.' in '.$class->class_room;?></strong></h6>
		</div>
	<!-- Add student -->
		<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4">
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD Student </label>
				</div>
				<form action="navigate?c_id=<?=$class_id;?>" method="post">
					<div class="mt-1">
		  				<input class="form-control" type="text" name="student_email" placeholder="Student's Email" required=""></div>
		  			<br />
		  				<input class="btn btn-primary pull-right" type="submit" name="add_student" value="ADD">
		  			<br><br>
				</form>
    		</div>
    	</div>
		<?php } ?>
	</div>
  </div>
</section>


<?php include ('../../main/footer.php'); ?>