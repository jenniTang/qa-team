<?php

    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../../Session.php';
    Session::init();
       include ('../../functions.php');
        Session::checkSession();
        $function = new Functions();


        $pro_id = Session::get("profile_id");
        $userData = $function->getData($pro_id,'qa_profile','profile');
        $userData2 = $function->getData($pro_id,'qa_users','profile');
        $user_id = $userData2->user_id; 
        $user_id_e = $function->e($user_id);
        $pro_id_e = $function->e($pro_id );

?>



 
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Teacher</title>
    <link rel="icon" href="../../lib/qaL.png">
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- My CSS -->
    <link href="../../css/style.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
     <!-- Daterange picker -->
    <link rel="stylesheet" href="../../css/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../css/adminLTE/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../../css/adminLTE/css/skins/_all-skins.min.css">
    <!-- jquery -->
    <script type="text/javascript" src="../../css/jquery/jquery-1.10.2.min.js"></script>
    <!-- datatables -->
    <link rel="stylesheet" href="../../css/datatable/css/jquery.dataTables.min.css">
    <!-- w3.css -->
    <link href="../../css/w3.css" rel="stylesheet">
    <!-- ckeditor/richtext -->
    <script src="../../css/ckeditor/ckeditor.js"></script>
    <script src="../../css/datatable/js/jquery-1.12.4.js"></script>
    <script src="../../css/datatable/js/jquery.dataTables.min.js"></script>
    
    <!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable( {
            "scrollY":        "350px",
            "scrollCollapse": true,
            "paging":         true
        } );
    } );
    </script> -->
    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
      
      .mon{
         background-color: #ffff90;
         border: 1px solid grey;
      }
      .mon2{
        background-color: white;
      }
      .margin-1{
        margin-left: 1em;
      }
      .margin-2{
        margin-left: 2em;
      }
      .margin-b{
        margin-right: 1em;
      }
      .post-frame{
        border: 1px solid #e1e2b5;
        background-color: #fdfddd;
      }
      .frame-fix{
        border-bottom: 1px solid #e8e9c8;
      }
      .comment-frame{
        background-color: #ffffc9; 
        border: 1px solid #e1e2b5; 
        margin-top: 1em;
        border-radius: 1px 1px 1px 1px;
        box-shadow: inherit 0 1px 10px 3px rgba(0, 0, 0, 0.075);
      }
      .comment-success{
        background-color: rgba(0,160,35,0.09); 
        border: 1px solid green; 
        margin-top: 1em;
        border-radius: 1px 1px 1px 1px;
        box-shadow: inherit 0 1px 10px 3px rgba(0, 0, 0, 0.075);
      }
      
    /******settings*******/
      .padd{
        padding: 10px;
      }
      .edit-ico{
        color: green;
        border: 1px solid green;
        padding: 5px;
        border-radius: 3px;
      }
      .edit-ico:hover{
        color: white;
        background-color: green;
      }
      .trash-ico{
        color: red;
        border: 1px solid red;
        padding: 5px;
        border-radius: 3px;
      }
      .trash-ico:hover{
        color: white;
        background-color: red;
      }
      .envelope-ico{
        color: orange;
        border: 1px solid orange;
        padding: 5px;
        border-radius: 3px; 
      }
      .envelope-ico:hover{
        color: white;
        background-color: orange;
      }
      
      .menu-font{
        color: green !important;
        text-shadow: 0px 2px 5px #add;
      }
    </style>

  </head>

<body class="hold-transition skin-black-light sidebar-mini">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top"  style="background-color: #ffff67; border: 1px solid rgba(0, 0, 0, 0.20);">
      <div class="container">
        <!-- Sidebar Button -->
        <a href="#" class="navbar-toggler" data-toggle="push-menu" role="button">
          <span><!-- <img src="../../lib/qaL-grey.png" height="22" width="23"> --> <i class="fa fa-chevron-down">  </i></span></a>
        <a class="navbar-brand" href="#"> 
           <?php
                  if (isset($userData)) {
                      echo $userData->fname." ".$userData->lname;
                  }

              ?>
        </a>
        <!-- Nav-button -->
        <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse text-center" id="navbarResponsive">
          <ul class="navbar-nav ml-auto nav-menu" id="qa-nav" >
            <li><a class="nav-link" href="home"><i class="fa fa-home"> </i> Home</a>
            </li>
            <!-- <li><a class="nav-link" href="#"><i class="fa fa-gear"> </i> Settings</a>
            </li> -->
            <li><a class="nav-link" href="../../logout.php"><i class="fa fa-sign-out"> </i> Logout</a>
            </li>
          </ul>
        </div>
      </div>
  </nav>

 <div class="wrapper"> 
    <aside class="main-sidebar" style="background-color: #FFFF99;">
      <!-- sidebar: style can be found in sidebar.less -->
       <section class="sidebar"> 
          <!-- Sidebar user panel -->
          <div class="user-panel" style="margin-top: -3em;">
            <div class="">
              <img src="../../lib/ciict_logo.png" class="img-square"  height="200" width="200" alt="User Image">
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->

          <ul class="sidebar-menu">
            <li class="header" style="background-color: #ffff67;">
            </li>
            <li class="frame-fix"><a class="menu-font" href="profile"><i class="fa fa-circle-o"></i> <span>My Profile</span></a></li>
            <li class="frame-fix"><a class="menu-font" href="classes"><i class="fa fa-users"></i> <span>Classes</span></a></li>
            <li class="frame-fix"><a class="menu-font" href="library"><i class="fa fa-book"></i> <span>Books</span></a></li>
            <li class="frame-fix"><a class="menu-font" href="forum"><i class="fa fa-comment"></i> <span>Forum</span></a></li>
          </ul>
      </section>
    <!-- /.sidebar -->
    </aside>


    <!-- Page Content -->
    <div class="content-wrapper" style="background-color: #ffffd3;">
