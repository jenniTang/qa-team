<?php include ('header.php');?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>

<section class="content">
  
	<p class="form-control">
		<a href="home"><i class="fa fa-home"></i></a>:			
	</p>

  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">

  	<p><h4 class="text-center col-md-12"><b>CIICT STUDENTS</b></h4></p>

	<!-- User data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
	    	<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
	    	<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
				            <tr>
				            	<th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
				                <th>NAME</th>
				                <!-- <th>EMAIL</th> -->
				                <th>COURSE</th>
				                <!-- <th>YEAR</th>
				                <th>SECTION</th> -->
				                <th></th>
				            </tr>
				        </thead>
				        <tbody class="datashow">
							<?php								
								$data = $function->getAllEmail(1);
								$i = 0;
								foreach($data as $val):
							?>
							<tr>
								<?php
									$profile_id = $function->e($val['profile_id']);
									$data = $function->getData($function->d($profile_id),'qa_profile','profile');
									$data2 = $function->getData($data->add_info_id,'qa_add_info','add_info');
									$data3 = $function->getData($data2->course_id,'qa_courses','course'); 
									if($profile_id){$i += 1; 
								?>
									<td><a href="users-profile2?p_id=<?= $profile_id;?>"><?= $i;}?></a></td>									
									<td><a class="pull-left" href="users-profile2?p_id=<?= $profile_id;?>"><?= $data->fname;?> <?= $data->mname;?> <?= $data->lname;?></a></td>
									<!-- <td><a href="users-profile2?p_id=<?= $profile_id;?>"><?= $val['email'];?></a></td> -->
									<td><a class="pull-left" href="users-profile2?p_id=<?= $profile_id;?>"><?= $data3->course_name;?></a></td>
									<!-- <td><a class="pull-left" href="users-profile2?p_id=<?= $profile_id;?>"><?= $data2->year;?></a></td>
									<td><a href="users-profile2?p_id=<?= $profile_id;?>"><?= $data2->section;?></a></td>
									<td><a href="users-profile2?p_id=<?= $profile_id;?>"><?= $data2->status;?></a></td>-->
									<td><a class="frame-space" href="users-profile2?p_id=<?= $profile_id;?>"><i class="edit-ico fa fa-pencil"></i></a>
										<a class="frame-space" href='#'><i onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')" class='trash-ico fa fa-trash'></i></a>
										<a class="frame-space" href="students?p_id=<?=$profile_id;?>#mailer"><i class="envelope-ico fa fa-envelope"></i></a>
									</td>

									<!-- Deletion Permission -->
									<div id="deletion<?=$function->d($profile_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to delete this user?</p>
									 	<a class="btn btn-danger" href="#">Delete</a>
									 	<!-- navigate?delete=1&utype=1&u_id=<?=$val['user_id']?>&p_id=<?=$function->d($profile_id)?> -->
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')">Cancel</a>
									</div>
							</tr>
							<?php endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table>
					<br />
				</form>
			</div>
		</div>
	</div>


	<div class="col-lg-4 col-sm-6 col-md-6 col-xs-6">
		<p id="addnew"></p>
  		<p id="mailer"></p>
  	<?php
  		if(isset($_GET['p_id'])){
  			$profile_id = $function->d($_GET['p_id']);
  			$data = $function->getData($profile_id, 'qa_users', 'profile');
  	?>

  	<!-- MAILER -->
	<!-- <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12"> -->
		<div class="col-lg-12 panel info-body-md">
			<?php		
				$msg2 = Session::get("msg");
				if(isset($msg2)){
					echo $msg2;
					Session::set("msg", NULL);
				}
			?>
	    	<div class="panel-body mt-4">
	    		<form action="../../vendor/mailer.php?id=<?=$profile_id;?>&utype=1" method="post">
	    			<div class="text-center" >
						<label style="font-size: 20px; color: grey;"> Send Mail </label>
					</div>
	    			<input type="text" class="form-control" readonly="" name="email" value="<?= $data->email;?>">
	    			<br />
	    			<input type="text" class="form-control" name="subject" placeholder="Subject"><br />
	    			<textarea style="height: 100px;" name="msg" class="form-control" placeholder="Message here." required=""></textarea>
	    			<br />
	    			<input class="btn btn-primary pull-right" type="submit" name="send" value="SEND">
	    			<br /><br />
	    		</form>
	    	</div>
	    </div>
	    <!-- add new data btn -->
	    <div class="col-lg-12 panel info-body-md">
	    	<div class="panel-body mt-4">
	    		<a class="form-control btn btn-default" href="students#addnew">Add new student?</a>
	    		<br><br>
	    	</div>
	    </div>
	</div>


	<?php }else{ ?>

	<!-- Add new data -->
  		<div class="col-lg-12 panel info-body-md">
  			<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
  			<div class="panel-body mt-3">
				<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD New Student </label>
				</div>
		  		<form action="navigate?utype=1" method="post">
		  			
			  				<div class="mt-1">
				  				<input class="form-control" type="text" name="fname" placeholder="  First Name" required=""></div>
				  			<div class="mt-1">
				  				<input class="form-control" type="text" name="mname" placeholder="  Middle Name"></div>
				  			<div class="mt-1">
				  				<input class="form-control" type="text" name="lname" placeholder="  Last Name" required=""></div>
				  			<div class="col-lg-12 mt-1">
				                <div class="row">
				                  <select name="course" class="select  col-lg-6" id="qa-form" required="">
				                    <option value="">Course</option>
				                    <?php
				                    $course = $function->getAllData('qa_courses');
				                    foreach($course as $value):?>
				                    <option value="<?= $value['course_id']?>"><?= $value['course_name']." ".$value['major'];?></option>
				                    <?php endforeach; ?>
				                  </select>
				                  <select name="section" class="select col-lg-6" id="qa-form" required="">
				                    <option value="">Section</option>
				                    <option>A</option>
				                    <option>B</option>
				                    <option>C</option>
				                    <option>D</option>
				                    <option>E</option>
				                    <option>F</option>
				                  </select>
				                </div>
				            </div>
				            <div class="col-lg-12">
				                <div class="row">
				                  <select name="year" class="select col-lg-6" id="qa-form" required="">
				                    <option value="">Year Level</option>
				                    <option>First Year</option>
				                    <option>Second Year</option>
				                    <option>Third Year</option>
				                    <option>Fourth Year</option>
				                  </select>
				                  <select name="status" class="select col-lg-6" id="qa-form" required="">
				                    <option value="">Status</option>
				                    <option>Regular</option>
				                    <option>Irregular</option>
				                  </select>
				                </div>
				            </div>	
			  			</div>
			  			
				  			<div class="mt-1">
				  				<input class="form-control" type="text" name="email" placeholder="   Email" required=""></div>
				  			<div class="mt-1">
				  				<input class="form-control" type="password" name="pass" placeholder="   Password" required=""></div>
				  			<div class="mt-1">
				  				<input class="form-control" type="password" name="pass2" placeholder="   Confirm Password" required=""></div><br />
			  				<div class="pull-right">
								<input  class="btn btn-primary" id="default" type="submit" name="add_user" value="ADD"></div><br /><br />
						</div>
					</div>
					<br />
		  		</form>
	  		</div>
	  	</div>
  	</div>

  	<?php } ?>
  </div>

</section>
<?php include ('../../main/footer.php'); ?>