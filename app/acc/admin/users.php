<?php include ('header.php');?>
<script src="../../css/datatable/config/config_dttbl_3.js"></script>

<section class="content">

	<p class="form-control">
		<a href="home"><i class="fa fa-home"></i></a>:			
	</p>

  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
	
  	<p><h4 class="text-center col-md-12"><b>THE ADMINISTRATORS</b></h4></p>

	<!-- User data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>	
	    	<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%; height: auto !important;">
						<thead>
				            <tr>
				            	<th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
				                <th>NAME</th>
				                <th>EMAIL</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody class="datashow">
							<?php								
								$data = $function->getAllEmail(3);
								$i = 0;
								foreach($data as $val):
							?>
							<tr>
								<?php
									$profile_id = $function->e($val['profile_id']);
									$data = $function->getData($function->d($profile_id),'qa_profile','profile');
										if($profile_id){$i += 1;
								?>
									<td><a href="users-profile?p_id=<?= $profile_id;?>"><?= $i;?></a></td>
									
									<td><a class="pull-left" href="users-profile?p_id=<?= $profile_id;?>"><?= $data->fname;?> <?= $data->mname;?> <?= $data->lname;?></a></td>
									<td><a class="pull-left" href="users-profile?p_id=<?= $profile_id;?>"><?= $val['email'];?></a></td>
									<td><a href="users-profile?p_id=<?= $profile_id;?>"><i class="edit-ico fa fa-pencil"></i></a>
										<?php if($function->d($profile_id)!=Session::get("profile_id")){ ?> 
										<a href='#'><i onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')" class='trash-ico fa fa-trash'></i></a>
										<?php }?>
										<a href="users?p_id=<?=$profile_id;?>#mailer"><i class="envelope-ico fa fa-envelope"></i></a>
									</td>

									<!-- Deletion Permission -->
									<div id="deletion<?=$function->d($profile_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey">
										<p>Are you sure you want to delete this user?</p>
									 	<a class="btn btn-danger" href="navigate?delete=1&p_id=<?=$function->d($profile_id);?>&u_id=<?=$val['user_id'];?>&utype=3">Delete</a>
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')">Cancel</a>
									</div>
							</tr>
							<?php } endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table>
					<br />
				</form>
			</div>
		</div>
	</div>

	
  	<div class="col-lg-4 col-sm-6 col-md-6 col-xs-6">
  		<p id="addnew"></p>
  		<p id="mailer"></p>
  	<?php
  		if(isset($_GET['p_id'])){
  			$profile_id = $function->d($_GET['p_id']);
  			$data = $function->getData($profile_id, 'qa_users', 'profile');
  	?>
<!-- MAILER -->
	<!-- <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12"> -->
		<div class="col-lg-12 panel info-body-md">
			<?php		
				$msg2 = Session::get("msg");
				if(isset($msg2)){
					echo $msg2;
					Session::set("msg", NULL);
				}
			?>
	    	<div class="panel-body mt-4">
	    		<form action="../../vendor/mailer.php?id=<?=$profile_id;?>&utype=3" method="post">
	    			<div class="text-center" >
						<label style="font-size: 20px; color: grey;"> Send Mail </label>
					</div>
	    			<input type="text" class="form-control" readonly="" name="email" value="<?= $data->email;?>">
	    			<br />
	    			<input type="text" class="form-control" name="subject" placeholder="Subject"><br />
	    			<textarea style="height: 100px;" name="msg" class="form-control" placeholder="Message here." required=""></textarea>
	    			<br />
	    			<input class="btn btn-primary pull-right" type="submit" name="send" value="SEND">
	    			<br /><br />
	    		</form>
	    	</div>
	    </div>
	    <!-- add new data btn -->
	    <div class="col-lg-12 panel info-body-md">
	    	<div class="panel-body mt-4">
	    		<a class="form-control btn btn-default" href="users#addnew">Add new admin?</a>
	    		<br><br>
	    	</div>
	    </div>
	</div>


	<?php }else{ ?>


<!-- Add new data -->
  		<div class="col-lg-12 panel info-body-md">
  			<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
  			<div class="panel-body mt-3">
				<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD New Admin </label>
				</div>
		  		<form action="navigate?utype=3" method="post">		
		  				<div class="mt-2">
			  				<input class="form-control" type="text" name="fname" placeholder="  First Name" required=""></div>
			  			<div class="mt-1">
			  				<input class="form-control" type="text" name="mname" placeholder="  Middle Name"></div>
			  			<div class="mt-1">
			  				<input class="form-control" type="text" name="lname" placeholder="  Last Name" required=""></div>
			  			<div class="mt-1">
			  				<input class="form-control" type="text" name="email" placeholder="   Email" required=""></div>
			  			<div class="mt-1">
			  				<input class="form-control" type="password" name="pass" placeholder="   Password" required=""></div>
			  			<div class="mt-1">
			  				<input class="form-control" type="password" name="pass2" placeholder="   Confirm Password" required=""></div><br />
		  				<div class="pull-right">
							<input  class="btn btn-primary" id="default" type="submit" name="add_user" value="ADD"></div><br /><br />
		  		</form>
	  		</div>
	  	</div>
  	</div>
  	<?php } ?>


  </div>

</section>
<?php include ('../../main/footer.php'); ?>