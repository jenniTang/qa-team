<?php
    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../../Session.php';
    Session::init();
    include ('../../functions.php');
	$function = new Functions();

	//for the USERS add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_user'])){
	    $utype = $_GET['utype'];
	    $flag = $function->checkSignup($_POST);
	    if($flag == 1){
	    	$go = $function->goUsers($utype);
      		header("Location: ".$go);
	    }
	    else{
		    if($utype == 3){
		    	$profile_id = $function->addProfile($_POST, 0);
		    	$usrRegi = $function->userRegister($_POST, $profile_id, 3);
		    	Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Admin is added!</div>");
		    }
		    else if($utype == 2){
		    	$profile_id = $function->addProfile($_POST, 0);
		    	$usrRegi = $function->userRegister($_POST, $profile_id, 2);
		    	Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Teacher is added!</div>");
		    }
		    else{
		    	$add_info_id = $function->addInfo($_POST);
		    	$profile_id = $function->addProfile($_POST, $add_info_id);
		    	$usrRegi = $function->userRegister($_POST, $profile_id, 1);
		    	Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Student is added!</div>");
		    }
	    	if(isset($usrRegi)){
		    	if($usrRegi > 0){
		    		$go = $function->goUsers($utype);
			      	header("Location: ".$go);
		      	}
		      	else{
		      		Session::set("msg", $usrRegi);
		      		$go = $function->goUsers($utype);
			      	header("Location: ".$go);
		      	}
		  	}
		}	    	    
	}

	//for the CLASS add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_class'])){
		$user_id = $_GET['u_id'];
		$profile_id = $_GET['p_id'];
		$flag = $function->addClass($_POST, $user_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Class has been added!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
		
		header("Location: teach-class?u_id=".$function->e($user_id)."&p_id=".$profile_id);
	}

	//for the Subject Enrolled Add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_sub_enrolled'])){
		$user_id = $_GET['u_id'];
		$profile_id = $_GET['p_id'];
		$class_id = $function->d($_POST['classcode']);

		$flag = $function->addSubEnrolled($class_id, $user_id);
			if($flag == 1){
				$class = $function->getData($class_id, 'qa_class', 'class');
				$enrolled = $function->getData($class_id, 'qa_sub_enrolled', 'class');
				$flag2 = $function->addStudGA($class->sub_id, $user_id, $enrolled->sub_enrolled_id);
				if($flag2 == 1){
					Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Class has been added!</div>");
				}
				else{
					Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Grading Data.</div>");
				}
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong Subject Enrolled.</div>");
			}
		
		header("Location: stud-class?u_id=".$user_id."&p_id=".$profile_id);
	}


	// for adding student on teacher's record
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_student'])){
		$class_id = $_GET['c_id'];
		$student = $function->getData2($_POST['student_email'], 'qa_users', 'email');
		$stud_id = $student->user_id;

		$checkStud = $function->checkData($stud_id, 'qa_sub_enrolled', 'user_id', 'class_id ='.$class_id);
		
		if($checkStud==0){
			if($student->user_type_id==1){
				
				$flag = $function->addSubEnrolled($class_id, $stud_id);
					if($flag == 1){
						$class = $function->getData($class_id, 'qa_class', 'class');
						$enrolled = $function->getDataReg($class_id, 'qa_sub_enrolled', 'class', 'sub_enrolled_id');
						$flag2 = $function->addStudGA($class->sub_id, $stud_id, $enrolled->sub_enrolled_id);
						if($flag2 == 1){
							Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Student has been added!</div>");
						}
						else{
							Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Grading Data.</div>");
						}
					}
					else{
						Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Subject Enrolled.</div>");
					}	
			}
			else{
				Session::set("msg","<div class='alert alert-warning mt-4'><strong>Warning ! </strong> You are trying to add a none student.</div>");
			}
		}
		else{
			Session::set("msg","<div class='alert alert-warning mt-4'><strong>Warning ! </strong> The student already exist.</div>");
		}
		header("Location: teach-classroom?c_id=".$function->e($class_id)."&p_id=".$_GET['p_id']."&u_id=".$function->e($_GET['u_id']));
	}

	//for the SUBJECT add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_subject'])){
		$flag = $function->addSubject($_POST);
		if($flag == 1){
			Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Subject has been added!</div>");
		}
		else{
			Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
		}
		header("Location: subjects");
	}

	//for the COURSE add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_course'])){
		$flag = $function->addCourse($_POST);
		if($flag==1){
			Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Course has been added!</div>");
		}
		else{
			Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
		}
		header("Location: courses");
	} 

	//for the BOOK add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_book'])){
		$return = $function->addBook($_POST);
		if(isset($return)){
	    	if($return > 0){
	    		Session::set("msg", "<div class='alert alert-success mt-4'><strong>Success ! </strong>New Book has been added!</div>");
		      	header("Location: books");
	      	}
	      	else{
	      		Session::set("msg", "<div class='alert alert-danger mt-4'>".$return."</div>");
	      		header("Location: books");
	      	}
	  	}
	}

	//for the FORUM post
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_topic'])){
		$user_id = Session::get("user_id");
		$flag = $function->addForumTopic($user_id, $_POST);
		if($flag>0){
			Session::set("msg","<div class='alert alert-default mt-4'><i>Your topic has been posted</i></div>");
		}
		else{
			Session::set("msg","<div class='alert alert-default mt-4'><strong>Unable to post! </strong>Try again.</div>");
		}
		header("Location: forum-feeds");
	}

	//for the FORUM comment
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_comment'])){
		$user_id = Session::get("user_id");
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumComment($user_id, $_POST, $forum_topic_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Commented..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum-feeds#post".$function->e($forum_topic_id));
	}

	//for the FORUM reply
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_reply'])){
		$user_id = Session::get("user_id");
		$forum_comment_id = $_GET['fc_id'];
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumReply($user_id, $_POST, $forum_comment_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Replied..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum-feeds#post".$function->e($forum_topic_id));
	}

	// to read more comments
	// if(isset($_GET['mc'])){
	// 	$forum_topic_id = $_GET['ft_id'];
	// 	header("Location: forum?mc=1#post".$function->e($forum_topic_id));
	// }

	// for USER REQUEST
	if (isset($_GET['usrrequest'])) {
		$flag = $function->editRequest($_GET['u_id']);
		if($flag==1){
			Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong> User is now able to Login.</div>");
		}
		else{
			Session::set("msg2","<div class='alert alert-default mt-4'><strong>Unable to accept! </strong>Try again.</div>");
		}
		header("Location: user-request");
	}


	// posting announcements
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_announce'])){
		// $user_id = Session::get("user_id");
		// $flag = $function->addAnnounce($user_id, $_POST);
		$flag = $function->addAnnounce($_POST);
		if($flag>0){
			Session::set("msg","<div class='alert alert-default mt-4'><i>Your announcement has been posted</i></div>");
		}
		else{
			Session::set("msg","<div class='alert alert-default mt-4'><strong>Unable to post! </strong>Try again.</div>");
		}
		header("Location: announce");
	}


	// for the CHANGE PASS
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['change_pass'])){
		$user_id = $_GET['u_id'];
		$user_id_e = $function->e($user_id);
		$pass = $_POST['pass'];
		if($pass==$_POST['pass2']){
			$checkPass = $function->checkPassword($pass);
			if(strlen($pass) < 8){
		    	Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Password should consist atleast eight characters.</div>");
		    	header("Location: change-pass?u_id=".$user_id_e);
		    }
		    else{
				if($checkPass==false){
				$flag = $function->editPass($user_id, $pass);
					if($flag==1){
						header("Location: change-pass?cp=tru&u_id=".$user_id_e);
					}else{
						Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
						header("Location: change-pass?u_id=".$user_id_e);
					}
				}
				else{
					Session::set("msg3","<div class='alert alert-warning mt-4'><strong>Error ! </strong>Weak Password.</div>");
					header("Location: change-pass?u_id=".$user_id_e);
				}
			}
		}
		else{
			Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Confirm password please.</div>");
			header("Location: change-pass?u_id=".$user_id_e);
		}
	}

	//for the USERS edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_user'])){
	  	$profile_id = $_GET['p_id'];
	  	$usrRegi = $function->editProfile($profile_id,$_POST,1);
	  	Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Profile is updated!</div>");
		  	if(isset($usrRegi)){
		    	if($usrRegi > 0){
		    		if(isset($_GET['ai_id'])){
				  		$function->editInfo($_POST, $_GET['ai_id']);
				  	}
		      	}
		      	else{
		      		Session::set("msg", $usrRegi);
		      	}
		      	$go = $function->goUsersProfile($_GET['utype']);
		      	$n_profile_id = $function->e($profile_id);
		      	header("Location: ".$go."?p_id=".$n_profile_id."#SuccessUPDATE!");
		  	}
	}

	//for the CLASS edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_class'])){
		$class_id = $_GET['c_id'];
		$user_id = $_GET['u_id'];
		$profile_id = $_GET['p_id'];
		$flag = $function->editClass($_POST, $class_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Class has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
		
		header("Location: teach-class?u_id=".$function->e($user_id)."&c_id=".$class_id."&p_id=".$profile_id);
	}

	//for the BOOK edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_book'])){
		$book_id = $_GET['b_id'];
			if(isset($_FILES)){
				$book = $function->getData($book_id, 'qa_books', 'book');
				echo "nabasa gehap?";
				if(unlink($book->book_file)){
					$flag = $function->editBook2($_POST, $book_id);
					echo "ka perpek ba ^_^";
				}
				else{
					$flag = $function->editBook2($_POST, $book_id);
					echo "ka perpek ba bisan ^_^";
				}
			}
			else{
				$flag = $function->editBook($_POST, $book_id);
					echo "LUOYA";	
			}

			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>The Book has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
			$msg = Session::get('msg');
			echo $msg;
			// header("Location: books?b_id=".$book_id."#viewdetails");
	}

	//for the SUBJECT edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_subject'])){
		$sub_id = $_GET['s_id'];
		$flag = $function->editSubject($_POST, $sub_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Subject has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
			header("Location: subjects?s_id=".$sub_id."#viewdetails");
	}

	//for the COURSE edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_course'])){
		$course_id = $_GET['c_id'];
		$flag = $function->editCourse($_POST, $course_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Course has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
			header("Location: courses?c_id=".$course_id."#viewdetails");
	}


	//for the Grade edit Teacher Management
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_grade'])){
		$stud_ga_id = $_GET['sg_id'];
		$user_id = $_GET['u_id'];
		$class_id = $_GET['c_id'];
		$profile_id_e = $_GET['p_id'];
		$stud_ga_id_e = $function->e($stud_ga_id);

		$flag = $function->editGrade($_POST, $stud_ga_id);
			if($flag == 1){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Student's grade has been updated!</div>");
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
			}
			header("Location: teach-classroom?u_id=".$user_id."&c_id=".$class_id."&p_id=".$profile_id_e."&sg_id=".$stud_ga_id_e);
	}

	//for the USERS sendmail
	if(isset($_GET['sentmail'])){
		if($_GET['sentmail']==1){
			Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Message has been sent!</div>");
		}
		else{
			Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Message not sent!</div>");
		}
		$profile_id = $_GET['id'];
		$profile_id_e = $function->e($profile_id);
	  	$go = $function->goUsers($_GET['utype']);
		header("Location: ".$go."?p_id=".$profile_id_e."#mailer");
	}

	//for the USERS, SUBJECT, COURSE, BOOK & CLASS delete
	if(isset($_GET['delete'])){
		$del = $_GET['delete'];

			// [1] USERS delete	
		  	if($del==1){
			  	$profile_id = $_GET['p_id'];
			  	$user_id = $_GET['u_id'];
			  	$utype = $_GET['utype'];
			  	

			  	// [1.2] Deleting a Teacher
			  	if($utype==2){
			  		$class = $function->getAllData('qa_class');
			  		foreach($class as $val){
			  			if($val['user_id']==$user_id){
			  				$stud = $function->getAllData('qa_sub_enrolled');
							foreach ($stud as $val2){
								if($val2['class_id']==$val['class_id']){
									$function->delete('qa_stud_ga', 'sub_enrolled', $val2['sub_enrolled_id']);
									$function->delete('qa_sub_enrolled', 'sub_enrolled', $val2['sub_enrolled_id']);
								}	
							}
							$function->delete('qa_class', 'class', $val['class_id']);
			  			}
			  		}
			  		$function->delete('qa_profile','profile', $profile_id);
			  		$function->delete('qa_users','user', $user_id);

			  		Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A user has been removed!</div>");
			  		header("Location: teachers");
			  	}

			  	if ($utype==3){
			  		$function->delete('qa_profile', 'profile', $profile_id);
			  		$function->delete('qa_users, user', $user_id);
			  		header("Location: users");
			  	}

			  	//[1.4] Deleteing of USER REQUEST: TEACHER
			  	if($utype==4){
			  		$profile = $function->getData($profile_id, 'qa_profile', 'profile');
			  		$function->delete('qa_add_info', 'add_info', $profile->add_info_id);
			  		$function->delete('qa_profile', 'profile', $profile_id);
			  		$function->delete('qa_users', 'user', $user_id);
			  		Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A user has been removed!</div>");
			  		header("Location: user-request");
			  	}

			  	//[1.5] Deleteing of USER REQUEST: TEACHER
			  	if($utype==5){
			  		$function->delete('qa_profile', 'profile', $profile_id);
			  		$function->delete('qa_users', 'user', $user_id);
			  		Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A user has been removed!</div>");
			  		header("Location: user-request");
			  	}
			  	


			  		// NOTE: theres more to delete when student
			  	// 	$add_info_id = $_GET['ai_id'];
			  	// 	$function->delete('qa_add_info','add_info', $add_info_id);
			  	// 	$go = 'students';
			  	// }
			  	// if(isset($utype)){
			  	// 	$go = $function->goUsers($utype);
			  	// }
			  	// header("Location: ".$go);
			}

			// [2] SUBJECT delete
			if($del==2){
				if(isset($_GET['s_id'])){
					$sub_id = $_GET['s_id'];
					$function->delete('qa_subjects', 'sub', $sub_id);
					Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A subject has been deleted!</div>");
					header("Location: subjects");
				}
			}


			// [3] COURSE delete
			if($del==3){
				if(isset($_GET['c_id'])){
					$course_id = $_GET['c_id'];
					$function->delete('qa_courses', 'course', $course_id);
					Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A course has been deleted!</div>");
					header("Location: courses");
				}
			}


			// [4] BOOK delete
			if($del==4){
				if(isset($_GET['b_id'])){
					$book_id = $_GET['b_id'];
					$file = $_GET['file'];
					$function->delete('qa_books', 'book', $book_id);
					if(unlink($file)){
						Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>A book has been deleted!</div>");
						header("Location: books");
					}
				}
			}


			// [5] CLASS delete
			if($del==5){
				$opt = $_GET['opt'];

				// [5.1] Student will be remove in the class on Teacher's Record 
				if($opt==1){
					$stud_ga_id = $function->d($_GET['sg_id']);
					$studGA = $function->getData($stud_ga_id, 'qa_stud_ga', 'stud_ga');
					$function->delete('qa_sub_enrolled', 'sub_enrolled', $studGA->sub_enrolled_id);
					$function->delete('qa_stud_ga', 'stud_ga', $stud_ga_id);

					Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A student has been removed!</div>");
					header("Location: teach-classroom?u_id=".$_GET['u_id']."&c_id=".$_GET['c_id']."&p_id=".$_GET['p_id']);
				}

				// [5.2] Certain class will be deleted
				if($opt==2){
					$class_id = $_GET['c_id'];
					$class = $function->getAllData('qa_sub_enrolled');
					foreach ($class as $val){
						if($val['class_id']==$class_id){
							$function->delete('qa_stud_ga', 'sub_enrolled', $val['sub_enrolled_id']);
							$function->delete('qa_sub_enrolled', 'sub_enrolled', $val['sub_enrolled_id']);
						}	
					}
					$function->delete('qa_class', 'class', $class_id);

					Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A class has been deleted!</div>");
					header("Location: teach-class?u_id=".$_GET['u_id']."&p_id=".$_GET['p_id']);
				
				}

				// [5.3] Remove a certain class on student class schedule
				if($opt==3){
					$sub_enrolled_id = $_GET['es_id'];
							$function->delete('qa_stud_ga', 'sub_enrolled', $sub_enrolled_id);
							$function->delete('qa_sub_enrolled', 'sub_enrolled', $sub_enrolled_id);

					Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A class has been deleted!</div>");
					header("Location: stud-class?p_id=".$_GET['p_id']);
				}

			}
	}
	

?>