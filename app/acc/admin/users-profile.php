<?php
include ('header.php');
$profile_id = $function->d($_GET['p_id']);
$data = $function->getData($profile_id,'qa_profile','profile');
$data2 = $function->getData($profile_id,'qa_users','profile');
$utype = $data2->user_type_id;
$user_id = $data2->user_id; 
$user_id_e = $function->e($user_id); 
?>

<section class="content">

	<p class="form-control">
		<a href="home"><i class="fa fa-home"></i></a>:
		<?php if($utype==2){ ?><a href="teachers">Teachers</a>\<?php }else{?><a href="users">Admins</a>\<?php }?>	
	</p>

	<?php if($utype==2){?>
	<div class="row offset-lg-2 offset-md-2 offset-sm-2 offset-xs-1">
  		<!-- User Profile -->
  	    <div class="col-lg-5 col-md-9 col-sm-9 col-xs-8">
	<?php }else{ ?>

  	<div class="row offset-lg-4 offset-md-2 offset-sm-2 offset-xs-1">
  		<!-- User Profile -->
  	    <div class="col-lg-6 col-md-9 col-sm-9 col-xs-8">
  	<?php } ?>
	    	<div class="col-lg-12 panel info-body-md">
	    		<?php
	    			$msg = Session::get("msg");
	    			if(isset($msg)){
	    				echo $msg;
	    				Session::set("msg", NULL);
	    			}
	    		?>
		    	<div class="panel-body mt-4">
		    		<div class="text-center">
						<label style="font-size: 20px; color: grey;"> User Profile </label>
					</div>
		    		<form action="navigate?p_id=<?=$profile_id;?>&utype=<?=$utype;?>" method="post">
						<h6>First Name:</h6>
						<input type="text" class="form-control" name="fname" value="<?= $data->fname;?>"><br />
						<h6>Middle Name:</h6>
						<input type="text" class="form-control" name="mname" value="<?= $data->mname;?>"><br />
						<h6>Last Name:</h6>
						<input type="text" class="form-control" name="lname" value="<?= $data->lname;?>">
						<br />
						<a class="form-control btn btn-default" href="change-pass?u_id=<?=$user_id_e;?>">CHANGE PASSWORD?</a>
						<br /><br />
						<div class="pull-right">
							<!-- <a class="btn btn-danger" href="navigate?delete=1&p_id=<?=$profile_id;?>&u_id=<?=$user_id;?>&utype=<?=$utype;?>">DELETE</a> -->
							<input class="btn btn-success" type="submit" name="update_user" value="UPDATE">
						</div>
						<br /><br />
					</form>
		    	</div>
		    </div>
		</div>

		<?php if($utype==2){  
		    	$profile_id_e = $function->e($profile_id);
		    	?>
		    <div class="col-lg-4 col-md-9 col-sm-9 col-xs-8">
		    <div class="col-lg-12 panel info-body-md">
		    	<div class="panel-body mt-4">
					<a class="form-control btn btn-default" href="teach-class?u_id=<?=$user_id_e;?>&p_id=<?= $profile_id_e;?>">SUBJECTS / CLASS</a>
				</div><br />
			</div>
			<?php } ?>
  	</div>
</section>

<?php include ('../../main/footer.php'); ?>