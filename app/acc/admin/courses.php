<?php include ('header.php'); ?>
<script src="../../css/datatable/config/config_dttbl_3.js"></script>

<section class="content">
  <div class="row offset-lg-0 offset-md-0 offset-xs-0">
  	<p><h4 class="text-center col-md-12"><b>CIICT COURSES</b></h4></p>
    <!-- Course data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
								<th>COURSE</th>
								<th>MAJOR</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$data = $function->getAllDAta('qa_courses');
							$i = 0;
							foreach($data as $val):
								$major = $val['major'];
								$course_id = $val['course_id'];
								if($course_id){ $i+=1; ?>
								<tr><td><a href="courses?c_id=<?=$course_id;?>#viewdetails"><?=$i;}?></a></td>
									<td><a class="pull-left" href="courses?c_id=<?=$course_id;?>#viewdetails"><?=$val['course_name'];?></a></td>
									<td><a class="pull-left" href="courses?c_id=<?=$course_id;?>#viewdetails"><?=($major=="")?'NONE':$major;?></a></td>
									<td><a href="#"></a></td>
									 
									<!-- Deletion Permission -->
									<div id="deletion<?=$function->d($profile_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to delete this user?</p>
									 	<a class="btn btn-danger" href="navigate?delete=1&utype=2&u_id=<?=$val['user_id']?>&p_id=<?=$function->d($profile_id)?>">Delete</a>
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')">Cancel</a>
									</div>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr><th></th></tr>
						</tfoot>
					</table>
					<br />
				</form>
			</div>
		</div>
	</div>
	


	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
		<!-- to jump in -->
		<p id="viewdetails"></p>
		<p id="addnew"></p>

    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
    		<?php if(isset($_GET['c_id'])){
    			$course_id = $_GET['c_id'];
    			$data3 = $function->getData($course_id,'qa_courses','course');
    			$major = $data3->major;
    		 ?>
    		<!-- Course Edit -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> EDIT Course </label>
				</div>
				<form action="navigate?c_id=<?=$course_id;?>" method="post">
					<div class="mt-1">
						<h6 class="pull-left">Course Name:</h6>
						<input class="form-control" type="text" name="cname" placeholder="    Course Name" value="<?=$data3->course_name?>">
					</div>
					<div class="mt-1">
						<h6 class="pull-left">Major:</h6>
						<input class="form-control" type="text" name="major" placeholder="    Major" value="<?=($major=="")?'NONE':$major;?>">
					</div>
					<div class="mt-3 pull-right">
						<a class="btn btn-danger" href="navigate?delete=3&c_id=<?=$course_id;?>">DELETE</a>
						<input class="btn btn-success" type="submit" name="update_course" value="UPDATE">
					</div>
					<br><br><br>
				</form>

    		<?php }else{ ?>
    		
    		<!-- Course Add -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD Course </label>
				</div>
				<form action="navigate" method="post">
					<div class="mt-1">
						<input class="form-control" type="text" name="cname" placeholder="    Course Name" required="">
					</div>
					<div class="mt-1">
						<input class="form-control" type="text" name="major" placeholder="    Major">
					</div>
					<div class="mt-3 pull-right">
						<input class="btn btn-primary" type="submit" name="add_course" value="ADD">
					</div>
					<br><br><br>
				</form>
			<?php } ?>
			</div>
		</div>

		<?php if(isset($_GET['c_id'])){ ?>
		<div class="col-lg-12 panel info-body-md">
			<div class="mt-4">
				<a class="btn btn-default form-control" href="courses#addnew">add new course?</a>
				<br><br>
			</div>
		</div>
		<?php } ?>

	</div>
  </div>
</section>

<?php include ('../../main/footer.php'); ?>