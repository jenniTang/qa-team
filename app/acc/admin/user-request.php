<?php include ('header.php');?>

<script src="../../css/datatable/config/config_dttbl_1.js"></script>

<section class="content">
  
	<p class="form-control">
		<a href="home"><i class="fa fa-home"></i></a>:			
	</p>

	
  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
	
  	<p><h4 class="text-center col-md-12"><b>USERS TO VERIFY</b></h4></p>

	<!-- User data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
	    	<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
	    	<div class="panel-body mt-4">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
				            <tr class="text-center">
				            	<th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
				                <th>NAME</th>
				                <th>EMAIL</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody class="datashow">
							<?php								
								$data = $function->getAllData('qa_users');
								$i = 0;
			 					foreach($data as $val):
								 if($val['user_verify']==0){
							?>
							<tr>
								<?php
								   $profile_id = $function->e($val['profile_id']);
								   $data = $function->getData($function->d($profile_id),'qa_profile','profile');
									if($profile_id){$i += 1; 
								?>
									<td class="text-center"><a href="users-profile?p_id=<?= $profile_id;?>"><?= $i;}?></a></td>
									<td><a class="pull-left" href="users-profile?p_id=<?= $profile_id;?>"><?= $data->fname;?> <?= $data->mname;?> <?= $data->lname;?></a></td>
									<td><a class="pull-left" href="users-profile?p_id=<?= $profile_id;?>"><?= $val['email'];?></a></td>
									<td class="pull-right"><a class="frame-space" href="user-request?p_id=<?=$profile_id?>#review"><i class="edit-ico fa fa-book"></i></a>
										<a class="frame-space edit-ico" href="navigate?usrrequest=1&u_id=<?=$val['user_id']?>">ACCEPT</a>
										<a class="frame-space trash-ico" href='#' onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')">DECLINE</a>
									</td>

									<!-- Deletion Permission -->
									<div id="deletion<?=$function->d($profile_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to decline this request of <?= $data->fname;?> <?= $data->mname;?> <?= $data->lname;?>?</p>
									 	<a class="btn btn-danger" href="navigate?delete=1&utype=<?=$val['user_type_id']+3?>&u_id=<?=$val['user_id']?>&p_id=<?=$function->d($profile_id)?>">Yes</a>
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->d($profile_id);?>')">Cancel</a>
									</div>
							</tr>
							<?php } endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table>
					<br />
				</form>
			</div>
		</div>
	</div>

	<!-- Profile -->
	<div class="col-lg-4 col-sm-6 col-md-6 col-xs-6">
		<p id="review"></p>
  	<?php
  		if(isset($_GET['p_id'])){
  			$profile_id = $function->d($_GET['p_id']);
  			$usr_users = $function->getData($profile_id, 'qa_users', 'profile');
  			$usr_prof  = $function->getData($profile_id, 'qa_profile', 'profile');

  	?>
			
	    	<div class="comment-frame col-lg-12">
	    		<h6>User type: <br><strong class="margin-1"><?=($usr_users->user_type_id==1)?"Student":"Teacher";?></strong></h6>
	    		  <div class="col-md-12">
	    			<img style="width: 100%;" src="../../<?=$usr_prof->profile_image;?>" ></div>
	    		<h6>Email: <br><strong class="margin-1"><?=$usr_users->email;?></strong></h6>
	    		<h6>Full name: <br><strong class="margin-1"><?= $usr_prof->fname;?> <?= $usr_prof->mname;?> <?= $usr_prof->lname;?></strong></h6>
	    	</div>
	</div>

	<?php } ?>


  	</div>
  </div>

</section>


<?php include ('../../main/footer.php'); ?>