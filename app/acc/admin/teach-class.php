<?php include ('header.php');
$user_id = $function->d($_GET['u_id']);
$user_id_e = $function->e($user_id);
$profile_id = $function->d($_GET['p_id']);
$profile_id_e = $function->e($profile_id);
$teacher = $function->getData($profile_id, 'qa_profile', 'profile');
?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>



<section class="content">

	<p class="form-control">
		<a href="home"><i class="fa fa-home"></i></a>:
		<a href="teachers">Teachers</a>\
		<a href="users-profile?p_id=<?= $profile_id_e;?>">Profile</a>\			
	</p>
  
  <div class="row offset-lg-0 offset-md-0 offset-xs-0">

  	<p><h4 class="text-center col-md-12">Classes of <b><?=$teacher->fname;?> <?=($teacher->mname==NULL)?'':$teacher->mname[0].'.';?> <?=$teacher->lname;?></b></h4></p>

	<!-- Class data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th>CLASS CODE</th>
								<th>SUBJECT</th>
								<th>CLASS</th>
								<th></th>
								<!-- <th>TIME</th>
								<th>DAY</th>
								<th>ROOM</th> -->
							</tr>
						</thead>
						<tbody class="datashow">
							<?php
								$data = $function->getAllData('qa_class');
								foreach($data as $val):
									if($val['user_id']==$user_id){
									$data2 = $function->getData($val['course_id'],'qa_courses','course');
									$data3 = $function->getData($val['sub_id'],'qa_subjects','sub');
									$class_id = $val['class_id'];
							?>
							<tr><td><a class="pull-left" href="teach-class?u_id=<?=$user_id_e;?>&c_id=<?=$class_id;?>&p_id=<?=$profile_id_e;?>#viewdetails"><?=$function->e($class_id);?></a></td>
								<td><a class="pull-left" href="teach-class?u_id=<?=$user_id_e;?>&c_id=<?=$class_id;?>&p_id=<?=$profile_id_e;?>#viewdetails"><?=$data3->sub_name?></a></td>
								<td><a class="pull-left" href="teach-class?u_id=<?=$user_id_e;?>&c_id=<?=$class_id;?>&p_id=<?=$profile_id_e;?>#viewdetails"><?=$data2->course_name." ".$val['year']." ".$val['section'];?></a></td>
								<!-- <td><a href="#"><?=$val['class_time'];?></a></td>
								<td><a href="#"><?=$val['class_day'];?></a></td>
								<td><a href="#"><?=$val['class_room'];?></a></td> -->
								<td>
									<a class="frame-space" href="teach-class?u_id=<?=$user_id_e;?>&c_id=<?=$class_id;?>&p_id=<?=$profile_id_e;?>#viewdetails"><i class="edit-ico fa fa-pencil"></i></a>
									<a class="frame-space" href='#'><i onclick="myAccFunc('deletion<?=$function->d($class_id);?>')" class='trash-ico fa fa-trash'></i></a></td>

								<!-- Deletion Permission -->
									<div id="deletion<?=$function->d($class_id);?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to delete this class?</p>
									 	<a class="btn btn-danger" href="navigate?delete=5&c_id=<?=$class_id;?>&u_id=<?=$user_id_e;?>&opt=2&p_id=<?=$profile_id_e;?>">Delete</a>
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$function->d($class_id);?>')">Cancel</a>
									</div>
							</tr>
							<?php } endforeach; ?> 
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table><br>
    			</form>
    		</div>
    	</div>
    </div>

   
	
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    <!-- to jump in -->
		<p id="viewdetails"></p>
		<p id="addnew"></p>


    <!-- Add/Edit Class -->
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4">

    		<?php
		    if(isset($_GET['c_id'])){
			    $class_id = $_GET['c_id'];
				$class = $function->getData($class_id,'qa_class','class'); ?>
		<!-- Edit Class -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> Edit Class </label>
				</div>
				<form action="navigate?c_id=<?=$class_id;?>&u_id=<?=$user_id;?>&p_id=<?=$profile_id_e;?>" method="post">
					<div class="mt-2">
		  				<select name="subject" class="select  col-lg-12" id="qa-form" required="">
		  					<?php
		  					$subjects = $function->getAllData('qa_subjects');
		  					foreach($subjects as $val):
		  					$sub_id = $val['sub_id']; ?>
		  					<option <?=($sub_id==$class->sub_id)?'selected':'';?> value="<?=$sub_id;?>"><?=$val['sub_name'];?></option>
		  					<?php endforeach; ?>
		  				</select>
		  			</div>
		  			<div class="">
			  			<select name="course" class="select  col-lg-12" id="qa-form" required="">
		                    <?php
		                    $course = $function->getAllData('qa_courses');
		                    foreach($course as $val):
		                    $course_id = $val['course_id']; ?>
		                    <option <?=($course_id==$class->course_id)?'selected':'';?> value="<?=$course_id;?>"><?= $val['course_name']." ".$val['major'];?></option>
		                    <?php endforeach; ?>
		                </select>
		            </div>
		  			<div class="col-lg-12">
		                <div class="row">
		                  <select name="year" class="select col-lg-6" id="qa-form" required="">
		                    <option><?=$class->year;?></option>
		                    <option>First Year</option>
		                    <option>Second Year</option>
		                    <option>Third Year</option>
		                    <option>Fourth Year</option>
		                  </select>
		                  <select name="section" class="select col-lg-6" id="qa-form" required="">
		                    <option><?=$class->section;?></option>
		                    <option>A</option>
		                    <option>B</option>
		                    <option>C</option>
		                    <option>D</option>
		                    <option>E</option>
		                    <option>F</option>
		                  </select>
		                </div>
		  			</div>
		  			<div class="">
		  				<input class="form-control" type="text" name="time" placeholder="  Class Time" value="<?=$class->class_time;?>" required=""></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="day" placeholder="  Class Day" value="<?=$class->class_day;?>" required=""></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="room" placeholder="  Class Room" value="<?=$class->class_room;?>" required=""></div>
		  			<div class="mt-3 pull-right">
		  				<!-- <a class="btn btn-danger" href="navigate?delete=5&c_id=<?=$class_id;?>&u_id=<?=$user_id_e;?>&p_id=<?=$profile_id_e;?>">DELETE</a> -->
		  				<input class="btn btn-success" type="submit" name="update_class" value="UPDATE">
		  			</div>
		  			<br><br><br>
				</form>

			<?php }else{ ?>

		 <!-- Add Class -->
		 		<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD Class </label>
				</div>
	    		<form action="navigate?u_id=<?=$user_id;?>&p_id=<?=$profile_id_e;?>" method="post">
	    			<div class="mt-2">
		  				<select name="subject" class="select  col-lg-12" id="qa-form" required="">
		  					<option value="">Subjects</option>
		  					<?php
		  					$subjects = $function->getAllData('qa_subjects');
		  					foreach($subjects as $val): ?>
		  					<option value="<?=$val['sub_id'];?>"><?=$val['sub_name'];?></option>
		  					<?php endforeach; ?>
		  				</select>
		  			</div>
		  			<div class="">
			  			<select name="course" class="select  col-lg-12" id="qa-form" required="">
		                    <option value="">Course</option>
		                    <?php
		                    $course = $function->getAllData('qa_courses');
		                    foreach($course as $val):?>
		                    <option value="<?= $val['course_id'];?>"><?= $val['course_name']." ".$val['major'];?></option>
		                    <?php endforeach; ?>
		                </select>
		            </div>
		  			<div class="col-lg-12">
		                <div class="row">
		                  <select name="year" class="select col-lg-6" id="qa-form" required="">
		                    <option value="">Year Level</option>
		                    <option>First Year</option>
		                    <option>Second Year</option>
		                    <option>Third Year</option>
		                    <option>Fourth Year</option>
		                  </select>
		                  <select name="section" class="select col-lg-6" id="qa-form" required="">
		                    <option value="">Section</option>
		                    <option>A</option>
		                    <option>B</option>
		                    <option>C</option>
		                    <option>D</option>
		                    <option>E</option>
		                    <option>F</option>
		                  </select>
		                </div>
		  			</div>
		  			<div class="">
		  				<input class="form-control" type="text" name="time" placeholder="  Class Time" required=""></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="day" placeholder="  Class Day" required=""></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="room" placeholder="  Class Room" required=""></div>
		  			<br />
		  				<input class="btn btn-primary pull-right" type="submit" name="add_class" value="ADD">
		  			<br /><br />
	    		</form>
	    	<?php } ?>
			</div>
		</div>

		<?php if(isset($_GET['c_id'])){ $class_id = $_GET['c_id']; ?>
		
		<div class="col-lg-12 panel info-body-md">
			<div class="mt-4">
				<!-- View Class -->
				<a class="btn btn-default form-control" href="teach-classroom?u_id=<?=$function->e($user_id);?>&c_id=<?=$function->e($class_id);?>&p_id=<?=$profile_id_e;?>">View Class Record</a>
				<!-- Add Class BACK Button -->
				<a class="btn btn-default form-control mt-2" href="teach-class?u_id=<?=$function->e($user_id);?>&p_id=<?=$profile_id_e;?>#addnew">add new class?</a>
				<br><br>
			</div>
		</div>

		<?php } ?>

    </div>

  </div>
</section>

<?php include ('../../main/footer.php'); ?>