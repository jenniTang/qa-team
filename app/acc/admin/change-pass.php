<?php
include 'header.php';
$user_id = $function->d($_GET['u_id']);
$data = $function->getData($user_id,'qa_users','user');
$profile_id_e = $function->e($data->profile_id);
$data2 = $function->getData($data->profile_id, 'qa_profile', 'profile');
?>

<section class="content">
  	<div class="row offset-lg-0 offset-md-0 offset-xs-0">
  		<!-- User Profile -->
  		<div class="col-lg-4 col-md-2 col-sm-2"></div>
  		
  	    <div class="col-lg-4 col-md-8 col-sm-8 col-xs-12" style="margin-top: 5em;">
	  	    	
  	    		<p class="text-center form-control">
  	    			<i class="fa fa-user"></i>
  	    			<?=$data2->fname." ".$data2->lname;?>
  	    			<i class="fa fa-lock"></i><i class="fa fa-key"></i></p>

	  	    	<?php if(isset($_GET['cp'])){ ?>
	  				<br>
	  				<div class="text-center comment-success">
	  					<h2 class="mt-4">Successfully changed password!</h2><br>
	  					<a class="btn btn-default form-control" href="<?=($data->user_type_id==1)?'users-profile2':'users-profile';?>?p_id=<?=$profile_id_e;?>">Back to Profile</a>
	  					<br><br>
	  				</div>

	  			<?php }else{ ?>
		    	<div class="col-lg-12 panel info-body-md">
		    		<?php		
		    			$msg = Session::get("msg3");
		    			if(isset($msg)){
		    				echo $msg;
		    				Session::set("msg3", NULL);
		    			}
		    		?>
			    	<div class="panel-body mt-4">
			    		<div class="text-center">
							<label style="font-size: 20px; color: grey;"> Change Password </label>
						</div>
			    		<form action="navigate?u_id=<?=$user_id;?>" method="post">
							<h6>New Password:</h6>
							<input type="Password" class="form-control" name="pass" required="" placeholder="  Type Password"><br />
							<h6>Confirm Password:</h6>
							<input type="Password" class="form-control" name="pass2" required="" placeholder="  Retype Password"><br />
							<div class="pull-right">
								<a class="btn btn-default" href="<?=($data->user_type_id==1)?'users-profile2':'users-profile';?>?p_id=<?=$profile_id_e;?>">CANCEL</a>
								<input class="btn btn-success" type="submit" name="change_pass" value="UPDATE">
							</div>
							<br><br>
						</form>
					</div>
				</div>
		</div>
		<?php } ?>
	</div>
</section>

<?php include '../../main/footer.php'; ?>