<?php
	include ('header.php');
?>
<script src="../../css/datatable/config/config_dttbl_2.js"></script><script src="../../css/datatable/config/config_dttbl_2.js"></script>
<section class="content">

  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">

  	<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12"></div>
  	
    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
    	
    	<!-- POSTING ANNOUNCEMENTS -->
    	<div class="col-lg-12 panel info-body-md">
    		<div class="panel-body mt-3 text-center">
    			<div class="text-left">
					<label style="font-size: 20px; color: grey;"> Post an announcement: </label>
				</div>
				<form action="navigate" method="post">
					<textarea id="announce" class="form-control" style="height: 90px;" type="text" name="announce" placeholder="Type your announce here." required=""></textarea>
					<script>
						CKEDITOR.replace( 'announce' );
					</script>
					<br>
					<div class="pull-right">
						<input type="submit" class="btn btn-primary" name="add_announce" value="POST">
					</div>
					<br><br>
				</form>
			</div>
    	</div>
    	<?php
			$msg = Session::get("msg");
			if(isset($msg)){
				echo $msg;
				Session::set("msg", NULL);
			}

			$data = $function->getAllData('qa_announce');
			foreach($data as $announce):
				$timed =  $function->timeCompute($announce['announce_date']);
		?>

		<div class="col-lg-12 panel info-body-md" id="post<?=$function->e($announce_id);?>">
    		<div class="panel-body mt-3">
    			<div class="text-left">
					<p style="color: grey;"><strong>
						<label style="font-size: 20px; color: #000;">
						ANNOUNCEMENT!</label></strong> posted <?=$timed;?></p>
				</div>
		    	<div class="post-frame">
					<p class="margin-1"><?=$announce['announce'];?></p>
				</div>
			</div>
			<br>
		</div>
		<?php endforeach;?>
	</div>
	</div>
</div>



<?php include ('../../main/footer.php'); ?>