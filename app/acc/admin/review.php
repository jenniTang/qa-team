<?php
$filepath = realpath(dirname(__FILE__));
include_once $filepath.'/../../Session.php';
    Session::init();
    include ('../../functions.php');
	$function = new Functions();

//to view a pdf
	if(isset($_GET['bopen'])){
		$bid = $function->d($_GET['file']);
        $data = $function->getData($bid, 'qa_books', 'book');
        $PDFfilename = $data->book_file;
		$pdf = file_get_contents($PDFfilename);						
        
        header('Content-Type: application/pdf');
        header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Content-Length: '.strlen($pdf));
        header('Content-Disposition: inline; filename="'.basename($PDFfilename).'";');
        ob_clean(); 
        flush(); 
        echo $pdf;
	}

?>