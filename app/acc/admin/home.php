<?php include ('header.php'); ?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>

<section class="content">

<?php
    $loginmsg = Session::get("loginmsg");
    if (isset ($loginmsg)) {
        echo $loginmsg;
    }
    Session::set("loginmsg", NULL);
?>

	<div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 panel info-body-md">
				<div class="panel-body mt-3 text-center">
					<div class="text-center">
						<label style="font-size: 20px; color: grey;">POPULATION</label>
					</div>
					<table class="col-lg-12 mon">
						<tr>
							<th class="mon">USERs</th>
							<th class="mon">No's</th>
						</tr>
						<tr>
							<?php
								$sum = 0;
								$data = $function->getAllEmail(3);
								foreach($data as $val): 
									if(isset($val['email'])){
										$sum += 1;
									}
								endforeach; ?>
							<td class="mon2"><?="<span class='margin-1 pull-left'>Admin </span>";?></td>
							<td class="mon2"><?=$sum;?></td>
						</tr>
						<tr>
							<?php
								$sum1 = 0;
								$data = $function->getAllEmail(2);
								foreach($data as $val): 
									if(isset($val['email'])){
										$sum1 += 1;
									}
								endforeach; ?>
							<td class="mon2"><?="<span class='margin-1 pull-left'>Teacher </span>";?></td>
							<td class="mon2"><?=$sum1;?></td>
						</tr>
						<tr>
							<?php
								$sum2 = 0;
								$data = $function->getAllEmail(1);
								foreach($data as $val): 
									if(isset($val['email'])){
										$sum2 += 1;
									}
								endforeach; ?>
							<td class="mon2"><?="<span class='margin-1 pull-left'>Student </span>";?></td>
							<td class="mon2"><?=$sum2;?></td>
						</tr>
						<tr>
							<td>total growth</td>
							<td style="color: green"><?=$sum+$sum1+$sum2;?></td>
						</tr>
					</table>
					<br />
				</div>
			</div>
		</div>
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-lg-12 panel info-body-md">
		    	<div class="panel-body mt-4 text-center">
					<form action="" method="get">
						<table id="example" class="display" style="width:100%">
							<thead>
					            <tr>
					            	<th></th>
					                <th>NAME</th>
					                <th>EMAIL</th>
					                <th>USER TYPE</th>
					            </tr>
					        </thead>
					        <tbody class="datashow">
								<?php
									$d = $function->getAllData('qa_users');
									$i = 0;
									foreach($d  as $val): ?>
								<tr>
									<?php
										$id = $val['profile_id'];
										$data = $function->getData($id,'qa_profile','profile');
										$data2 = $function->getData($val['user_type_id'],'qa_user_type','user_type');
										if($id){$i += 1;
										?>
										<td><a href="#"><?= $i;}?></a></td>
										
										<td><a class="pull-left" href="#"><?= $data->fname;?> <?= $data->mname;?> <?= $data->lname;?></a></td>
										<td><a class="pull-left" href="#"><?= $val['email'];?></a></td>
										<td><a href="#"><?= $data2->user_type;?></a></td>
								</tr>
								<?php endforeach;?>
							</tbody>
							<tfoot>
								<tr>
									<th></th>
								</tr>
							</tfoot>
						</table>
						<br />
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include ('../../main/footer.php'); ?>