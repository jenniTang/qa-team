<?php include ('header.php'); ?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>

<section class="content">
  <div class="row offset-lg-0 offset-md-0 offset-xs-0">
  	<p><h4 class="text-center col-md-12"><b>CIICT SUBJECTS</b></h4></p>  	
    <!-- Subject data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
								<th>CODE</th>
								<th>SUBJECT</th>
								<th>UNIT</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$data = $function->getAllData('qa_subjects');
							$i = 0;
							foreach($data as $val):
								$sub_id = $val['sub_id'];
							if($sub_id){ $i+=1; ?>
							<tr><td><a href="subjects?s_id=<?=$sub_id;?>#viewdetails"><?=$i;}?></a></td>
								<td><a class="pull-left" href="subjects?s_id=<?=$sub_id;?>#viewdetails"><?=$val['sub_code'];?></a></td>
								<td><a class="pull-left" href="subjects?s_id=<?=$sub_id;?>#viewdetails"><?=$val['sub_name'];?></a></td>
								<td><a href="subjects?s_id=<?=$sub_id;?>#viewdetails"><?=$val['sub_unit'] ;?></a></td>
							</tr>
						<?php endforeach;?>
						</tbody>
						<tfoot>
							<tr><th></th></tr>
						</tfoot>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>
	

	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
		<!-- to jump in -->
		<p id="viewdetails"></p>
		<p id="addnew"></p>

    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
			<?php if(isset($_GET['s_id'])){
			$sub_id = $_GET['s_id'];
			$data3 = $function->getData($sub_id,'qa_subjects','sub'); ?>
	

    		<!-- Subject Edit -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> EDIT Subject </label>
				</div>
				<form action="navigate?s_id=<?=$sub_id;?>" method="post">
					<div class="mt-1">
						<h6 class="pull-left">Subject Code:</h6>
						<input class="form-control" type="text" name="code" placeholder="    Subject Code" value="<?=$data3->sub_code;?>" required="">
					</div>
					<div class="mt-1">
						<h6 class="pull-left">Subject Name:</h6>
						<input class="form-control" type="text" name="sname"  placeholder="    Subject Name" value="<?=$data3->sub_name;?>" required="">
					</div>
					<div class="mt-1">
						<h6 class="pull-left">Units:</h6>
						<input class="form-control" type="text" name="unit" placeholder="    Units" value="<?=$data3->sub_unit;?>" required="">
					</div>
					<div class="mt-3 pull-right">
						<a class="btn btn-danger" href="navigate?delete=2&s_id=<?=$sub_id;?>">DELETE</a>
						<input class="btn btn-success" type="submit" name="update_subject" value="UPDATE">
					</div>
					<br><br><br>
				</form>
			</div>
		</div>

		<div class="col-lg-12 panel info-body-md">
			<div class="mt-4">
				<a class="btn btn-default form-control" href="subjects#addnew">add new subject?</a>
				<br><br>
			</div>
		</div>
	</div>

	<?php }else{ ?>

			<!-- Subject Add -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD Subject </label>
				</div>
				<form action="navigate" method="post">
					<div class="mt-1">
						<input class="form-control" type="text" name="code" placeholder="    Subject Code" required="">
					</div>
					<div class="mt-1">
						<input class="form-control" type="text" name="sname" placeholder="    Subject Name" required="">
					</div>
					<div class="mt-1">
						<input class="form-control" type="text" name="unit" placeholder="    Units" required="">
					</div>
					<div class="mt-3 pull-right">
						<input class="btn btn-primary" type="submit" name="add_subject" value="ADD">
					</div>
					<br><br><br>
				</form>
			</div>
		</div>
	</div>
	<?php } ?>
  </div>
</section>

<?php include ('../../main/footer.php'); ?>