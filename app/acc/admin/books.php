<?php include ('header.php');?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>



<section class="content">
  
<div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
	<p><h4 class="text-center col-md-12"><b>ICT BOOKS</b></h4></p>

	<!-- Book data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th><i class="fa fa-long-arrow-up"><i class="fa fa-long-arrow-down"></i></i></th>
								<th>CATEGORY</th>
								<th>BOOK NAME</th>
								<th>AUTHOR</th>
								<!-- <th>DESCRIPTION</th>
								<th>PERMIT</th> -->
							</tr>
						</thead>
						<tbody class="datashow">
							<?php
								$data = $function->getAllData('qa_books');
								$i = 0;
								foreach($data as $val):
									$book_id = $val['book_id'];
									$data2 = $function->getData($val['sub_id'],'qa_subjects','sub');

									if($book_id){ $i += 1; ?>
							<tr><td><a href="books?b_id=<?=$book_id;?>#viewdetails"><?=$i;}?></a></td>
								<td><a class="pull-left" href="books?b_id=<?=$book_id;?>#viewdetails"><?=$data2->sub_name;?></a></td>
								<!--  target="_blank" -->
								<td><a class="pull-left" href="books?b_id=<?=$book_id;?>#viewdetails"><?=$val['book_name'];?></a></td>
								<td><a class="pull-left" href="books?b_id=<?=$book_id;?>#viewdetails"><?=$val['book_author'];?></a></td>
			   					<!-- <td><a href="#"><?=$val['book_description'];?></a></td>
								<td><a href="#"><?=$val['book_permit'];?></a></td> -->
							</tr>
							<?php endforeach;?>
						</tbody>
						<tfoot><tr><th></th></tr></tfoot>
					</table>
					<br>
    			</form>
    		</div>
    	</div>
    </div>


    <?php if(isset($_GET['b_id'])){
	    $book_id = $_GET['b_id'];
		$data = $function->getData($book_id,'qa_books','book');
		$PDFfilename = $data->book_file;
    ?>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<p id="viewdetails"></p>
    	<div class="col-lg-12 panel info-body-md">
    		<div class="panel-body">
		    	<p class="mt-4">View book:</p>
		    	
		    		<a class="btn btn-default form-control" target="_blank" href="review?bopen=1&file=<?=$function->e($data->book_id);?>"><?=$data->book_name;?></a>
		    	
		    		<!-- scripts: ../../main/footer.php -->
		    		<!-- <a onclick="openPdf('<?=$PDFfilename;?>', '<?=$data->book_name;?>')" href="#" class="btn btn-default col-md-12"><?=$data->book_name.".pdf";?></a> -->
		    		<br><br>
	    	</div>
    	</div>
		<div class="col-lg-12 panel info-body-md">
			<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<br>
    		<div class="panel-body mt-2 text-center">

    	<!-- Update book -->
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> EDIT Book </label>
				</div>
				<form action="navigate?b_id=<?=$book_id;?>" method="post">
					<div class="mt-1">
						<h6 class="pull-left">Book Category:</h6>
		  				<!-- <input class="form-control" type="text" name="bcategory" placeholder="  Book Category" value="<?=$data->book_category;?>" required=""></div> -->
		  				<select class="form-control select" type="text" name="bcategory" required="">
		  				<?php
		  					$subjects = $function->getAllData('qa_subjects');
		  					foreach($subjects as $val):
		  					$sub_id = $val['sub_id']; ?>
		  					<option <?=($sub_id==$data->sub_id)?'selected':'';?> value="<?=$sub_id;?>"><?=$val['sub_name'];?></option>
		  					<?php endforeach; ?>
		  				</select>
		  			</div>
					<div class="mt-1">
						<h6 class="pull-left">Book Name:</h6>
		  				<input class="form-control" type="text" name="bname" placeholder="  Book Name" value="<?=$data->book_name;?>" required=""></div><br>
		  			<div class="mt-1 form-control">
		  				<h6 class="pull-left"><i>Change PDF?</i></h6>
		  				<input class="input-group" type="file" name="book" /></div>
		  			<div class="mt-1">
		  				<h6 class="pull-left">Author:</h6>
		  				<input class="form-control" type="text" name="bauthor" placeholder=" Author" value="<?=$data->book_author;?>" required=""></div>
		  			<div class="mt-1">
		  				<h6 class="pull-left">Book descriptions:</h6>
		  				<textarea style="height: 100px;" class="form-control" type="text" name="bdescription" placeholder=" Book descriptions here." required=""><?=$data->book_description;?></textarea></div>
		  			<div class="mt-1">
		  				<h6 class="pull-left">Permit</h6>
		  				<input class="form-control" type="text" name="bpermit" placeholder="  Permit" value="<?=$data->book_permit;?>" required=""></div><br />
	  				<div class="pull-right">
	  					<a class="btn btn-danger" href="navigate?delete=4&b_id=<?=$data->book_id;?>&file=<?=$PDFfilename;?>">DELETE</a>
						<input  class="btn btn-success" id="default" type="submit" name="update_book" value="UPDATE"></div>
		  			<br /><br />
				</form>
    		</div>
    	</div>
    	<div class="col-lg-12 panel info-body-md">
    		<div class="mt-4">
    			<a class="btn btn-default form-control" href="books#addnew">add new book?</a>
    			<br><br>
    		</div>
    	</div>
	</div>

    <?php }else{ ?>

    <!-- Add book -->
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<p id="addnew"></p>
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
    			<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD New Book </label>
				</div>
				<form action="navigate" method="post" enctype="multipart/form-data">
					<div class="mt-1">
		  				<!-- <input class="form-control" type="text" name="bcategory" placeholder="  Book Category" required=""></div> -->
		  				<select class="form-control select" type="text" name="bcategory" required="">
		  					<option value="">Book Category</option>
		  					<?php
		  					$subjects = $function->getAllData('qa_subjects');
		  					foreach($subjects as $val):
		  					$sub_id = $val['sub_id']; ?>
		  					<option value="<?=$sub_id;?>"><?=$val['sub_name'];?></option>
		  					<?php endforeach; ?>
		  				</select>
					<div class="mt-1">
		  				<input class="form-control" type="text" name="bname" placeholder="  Book Name" required=""></div>
		  			<div class="mt-1">
		  				<input class="input-group" type="file" name="book" /></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="bauthor" placeholder=" Author" required=""></div>
		  			<div class="mt-1">
		  				<textarea style="height: 100px;" class="form-control" type="text" name="bdescription" placeholder=" Book descriptions here." required=""></textarea></div>
		  			<div class="mt-1">
		  				<input class="form-control" type="text" name="bpermit" placeholder="  Permit" required=""></div><br />
	  				<div class="pull-right">
						<input  class="btn btn-primary" id="default" type="submit" name="add_book" value="ADD"></div>
		  			<br /><br />
				</form>
			</div>
		</div>
	</div>
	<?php } ?>
</div>

</section>
<?php include ('../../main/footer.php'); ?>