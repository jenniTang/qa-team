<?php include ('header.php'); ?>

<section class="content">


  	<div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
  		<!-- User Profile -->
  	    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-lg-12 panel info-body-md">
	    		<?php
	    			$msg = Session::get("msg");
	    			if(isset($msg)){
	    				echo $msg;
	    				Session::set("msg", NULL);
	    			}
	    		?>
		    	<div class="panel-body mt-4">
		    		<div class="text-center">
						<label style="font-size: 20px; color: grey;"> My Profile </label>
					</div>
		    		<form action="navigate?p_id=<?=$pro_id;?>&ai_id=<?=$userData->add_info_id;?>" method="post">
		    			<div class="row">
		    				<div class="col-lg-6">
								<h6>First Name:</h6>
								<input type="text" class="form-control" name="fname" value="<?= $userData->fname;?>">
								<h6 class="mt-1">Middle Name:</h6>
								<input type="text" class="form-control" name="mname" value="<?= $userData->mname;?>">
								<h6 class="mt-1">Last Name:</h6>
								<input type="text" class="form-control" name="lname" value="<?= $userData->lname;?>">
							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
					                	<h6 class="col-lg-12">Course:</h6>
						                  <select name="course" class="select  col-lg-12" id="qa-form" required="">
						                    <!-- <option value="">Course</option> -->
						                    <?php
						                    $course = $function->getAllData('qa_courses');
						                    foreach($course as $value):
						                    $course_id =  $value['course_id']; ?>
						                    <option <?= ($course_id == $userData3->course_id)?'selected':''; ?> value="<?=$course_id;?>"><?= $value['course_name']." ".$value['major'];?></option>
						                    <?php endforeach; ?>
						                  </select>
							        </div>
							        <div class="col-lg-6">
					                	<h6 class="col-lg-12">Year:</h6>
						                  <select name="year" class="select col-lg-12" id="qa-form" required="">
						                    <option><?=$userData3->year;?></option>
						                    <option>First Year</option>
						                    <option>Second Year</option>
						                    <option>Third Year</option>
						                    <option>Fourth Year</option>
						                  </select>
							        </div>
							        <div class="col-lg-6">
					                	<h6 class="col-lg-12">Section:</h6>
						                  <select name="section" class="select col-lg-12" id="qa-form" required="">
						                    <option><?=$userData3->section;?></option>
						                    <option>A</option>
						                    <option>B</option>
						                    <option>C</option>
						                    <option>D</option>
						                    <option>E</option>
						                    <option>F</option>
						                  </select>
						            </div>    
							        <div class="col-lg-6">
					                	<h6 class="col-lg-12">Status:</h6>
						                  <select name="status" class="select col-lg-12" id="qa-form" required="">
						                    <option><?=$userData3->status;?></option>
						                    <option>Regular</option>
						                    <option>Irregular</option>
						                  </select>
						            </div>
						        </div>	
							</div>
						</div>

						<br />
						<a class="form-control btn btn-default" href="change-pass">CHANGE PASSWORD?</a>
						<br /><br />
						<div class="pull-right">
							<!-- <a class="btn btn-danger" href="navigate?delete=1&p_id=<?=$profile_id;?>&u_id=<?=$data2->user_id;?>&ai_id=<?=$data->add_info_id;?>&utype=<?=$data2->user_type_id;?>">DELETE</a> -->
							<input class="btn btn-success" type="submit" name="update_user" value="UPDATE">
						</div>
						<br /><br />
					</form>
		    	</div>
		    </div>
		</div>
	
  	</div>
</section>

<?php include ('../../main/footer.php'); ?>