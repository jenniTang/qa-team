<?php 
include ('header.php');
?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>

<section class="content">
  
  <div class="row offset-lg-0 offset-md-0 offset-xs-0">
  	<p><h3 class="text-center col-md-12"><b>GRADES</b></h3></p>
	<!-- Class data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th></th>
								<th>SUBJECT</th>
								<th>UNIT</th>
								<th>GRADE</th>
							</tr>
						</thead>
						<tbody class="datashow">
							<?php
							$grades = $function->getAllData('qa_stud_ga');
							$i = 0;
							foreach($grades as $val):
								if($user_id==$val['user_id']){
								$i += 1;
								$subject = $function->getData($val['sub_id'],'qa_subjects','sub');
							?>
							<tr><td><a href="#"><?=$i;?></a></td>
								<td><a class="pull-left" href="#"><?=$subject->sub_name;?></a></td>
								<td><a href="#"><?=$subject->sub_unit;?>.0</a></td>
								<td><a href="#"><?=$val['sub_grade'];?></a></td>
							</tr>
							<?php } endforeach;?>
						</tbody>
						<tfoot>
							<tr><th></th></tr>
						</tfoot>
					</table><br>
				</form>
			</div>
		</div>
	</div>
  </div>

</section>

<?php include('../../main/footer.php');?>