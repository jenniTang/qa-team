<?php
    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../../Session.php';
    Session::init();
    include ('../../functions.php');
	$function = new Functions();


	//for the PROFILE edit
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_user'])){
	  	$profile_id = $_GET['p_id'];
	  	$usrRegi = $function->editProfile($profile_id,$_POST,1);
	  	Session::set("msg","<div class='alert alert-success mt-4'><strong>Success ! </strong>Profile is updated!</div>");
		  	if(isset($usrRegi)){
		    	if($usrRegi > 0){
		    		if(isset($_GET['ai_id'])){
				  		$function->editInfo($_POST, $_GET['ai_id']);
				  	}
		      	}
		      	else{
		      		Session::set("msg", $usrRegi);
		      	}
		      	$profile_id_e = $function->e($profile_id);
		      	header("Location: profile#SuccessUPDATE!");
		  	}
	}

	// for the CHANGE PASS
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['change_pass'])){
		$user_id = $_GET['u_id'];
		$pass = $_POST['pass'];
		if($pass==$_POST['pass2']){
			$checkPass = $function->checkPassword($pass);
			if(strlen($pass) < 8){
		    	Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Password should consist atleast eight characters.</div>");
		    	header("Location: change-pass?u_id=");
		    }
		    else{
				if($checkPass==false){
				$flag = $function->editPass($user_id, $pass);
					if($flag==1){
						header("Location: change-pass?cp=143#SuccessChangePassword");
					}else{
						Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong.</div>");
						header("Location: change-pass");
					}
				}
				else{
					Session::set("msg3","<div class='alert alert-warning mt-4'><strong>Error ! </strong>Weak Password.</div>");
					header("Location: change-pass");
				}
			}
		}
		else{
			Session::set("msg3","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Confirm password please.</div>");
			header("Location: change-pass");
		}
	}

	//for the Subject Enrolled Add
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_sub_enrolled'])){
		$user_id = $_GET['u_id'];
		$class_id = $function->d($_POST['classcode']);
		$classes = $function->getAllData('qa_class');
		foreach ($classes as $val) {
			if($class_id==$val['class_id']){
				$i=0;
			}
		}
		if(isset($i)){
		  $flag = $function->addSubEnrolled($class_id, $user_id);
			if($flag == 1){
				$class = $function->getData($class_id, 'qa_class', 'class');
				$enrolled = $function->getData3($class_id, $user_id, 'qa_sub_enrolled', 'class', 'user');
				$flag2 = $function->addStudGA($class->sub_id, $user_id, $enrolled->sub_enrolled_id);
				if($flag2 == 1){
					Session::set("msg2","<div class='alert alert-success mt-4'><strong>Success ! </strong>New Class has been added!</div>");
				}
				else{
					Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong on Grading Data.</div>");
				}
			}
			else{
				Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Something went wrong Subject Enrolled.</div>");
			}
		}else{
			Session::set("msg","<div class='alert alert-danger mt-4'><strong>Error ! </strong>Class code does not exist.</div>");
		}
		
		header("Location: classes");
	}

		//for the FORUM post
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_topic'])){
		
			$user_id = Session::get("user_id");
			$flag = $function->addForumTopic($user_id, $_POST);
			if($flag>0){
				Session::set("msg","<div class='alert alert-default mt-4'><i>Your topic has been posted</i></div>");
			}
			else{
				Session::set("msg","<div class='alert alert-default mt-4'><strong>Unable to post! </strong>Try again.</div>");
			}
		//  

		header("Location: forum");
	}

	//for the FORUM comment
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_comment'])){
		$user_id = Session::get("user_id");
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumComment($user_id, $_POST, $forum_topic_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Commented..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum#post".$function->e($forum_topic_id));
	}

	//for the FORUM reply
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_reply'])){
		$user_id = Session::get("user_id");
		$forum_comment_id = $_GET['fc_id'];
		$forum_topic_id = $_GET['ft_id'];
		$flag = $function->addForumReply($user_id, $_POST, $forum_comment_id);
		if($flag>0){
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><i>Replied..</i></p>");
		}
		else{
			Session::set("msg<?=$forum_topic_id;?>","<p class='alert alert-default mt-2'><strong>Unable to comment! </strong>Try again.</p>");
		}
		header("Location: forum#post".$function->e($forum_topic_id));
	}


	//Remove a certain class on student class schedule
	if(isset($_GET['delete'])){
		$sub_enrolled_id = $_GET['se_id'];
				$function->delete('qa_stud_ga', 'sub_enrolled', $sub_enrolled_id);
				$function->delete('qa_sub_enrolled', 'sub_enrolled', $sub_enrolled_id);

		Session::set("msg2", "<div class='alert alert-success mt-4'><strong>Success ! </strong>A class has been removed!</div>");
		header("Location: classes");
	}

?>