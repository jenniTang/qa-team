<?php include ('header.php'); ?>
<script src="../../css/datatable/config/config_dttbl_1.js"></script>


<section class="content">


  <div class="row mt-1 offset-lg-0 offset-md-0 offset-xs-0">
  	<br><br>
  	<p><h3 class="text-center col-md-12"><b> Class Schedule <!-- <?=$student->fname?> <?=($student->mname==NULL)?'':$student->mname[0].'.';?> <?=$student->lname;?> --></b></h3></p>

  	<!-- Class data -->
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg2 = Session::get("msg2");
    			if(isset($msg2)){
    				echo $msg2;
    				Session::set("msg2", NULL);
    			}
    		?>
    		<div class="panel-body mt-4 text-center">
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr><th></th>
								<th>SUBJECT</th>
								<!-- <th>CLASS</th> -->
								<th>TIME</th>
								<th>INSTRUCTOR</th>
								<th></th>
								<!-- <th>DAY</th>
								<th>ROOM</th> -->
							</tr>
						</thead>
						<tbody class="datashow">
							<?php 
								$data = $function->getAllData('qa_sub_enrolled');
								$i = 0;
								foreach($data as $val):
									if($val['user_id']==$user_id){
										$i+=1;
										$class_id = $val['class_id'];
										$class = $function->getData($class_id,'qa_class','class');
										$subject = $function->getData($class->sub_id,'qa_subjects','sub');
										$teacher = $function->getData($class->user_id,'qa_users','user');
										$teacher2 = $function->getData($teacher->profile_id,'qa_profile','profile');
										$sub_enrolled_id = $val['sub_enrolled_id'];

								
							
										
							?>
							<tr>
								<td><a href="#"><?=$i;?></a></td>
								<td><a class="pull-left" href="stud-class?c_id=<?=$class_id;?>&u_id=<?=$user_id;?>&p_id=<?=$profile_id_e;?>"><?=$subject->sub_name;?></a></td>
								<td><a href="#"><?=$class->class_time;?></a></td>
								<td><a class="pull-left" href="#"><?= $teacher2->fname;?> <?= $teacher2->mname;?> <?= $teacher2->lname;?></a></td>
								<td>
									<a class="frame-space" href="classes?c_id=<?=$class_id;?>"><i class="edit-ico fa fa-book"></i></a>
									<a class="frame-space" href='#'><i onclick="myAccFunc('deletion<?=$class_id;?>')" class='trash-ico fa fa-trash'></i></a>
								</td>
								<?php $classsched = $function->getData3($user_id, $class->sub_id, 'qa_stud_ga', 'user', 'sub');
								?>
							
								<!-- Deletion Permission -->
									<div id="deletion<?=$class_id;?>" class="padd w3-display-middle w3-dropdown-content w3-card-4 w3-hide w3-dark-grey text-center">
										<p>Are you sure you want to remove this class <?=$subject->sub_name;?> ?</p>
										<!-- <?=$class_id." ".$i;?> <?=$classsched->stud_ga_id;?> -->
									 	<a class="btn btn-danger" href="navigate?delete=1&se_id=<?=$sub_enrolled_id?>">Remove</a>
									 	<!-- navigate?delete=1&utype=1&u_id=<?=$val['user_id']?>&p_id=<?=$function->d($profile_id)?> -->
									 	<a class="btn btn-default" href="#" onclick="myAccFunc('deletion<?=$class_id;?>')">Cancel</a>

									</div>
							</tr>
							<?php  } endforeach; ?> 
						</tbody>
						<tfoot>
							<tr>
								<th></th>
							</tr>
						</tfoot>
					</table><br>
    			</form>
    		</div>
    	</div>
    </div>

    <!-- Add/Edit Class -->
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<?php
	    	if(isset($_GET['c_id'])){
	    		$class_id = $_GET['c_id'];
	    		$class = $function->getData($class_id, 'qa_class', 'class');
	    		$class_subject = $function->getData($class->sub_id, 'qa_subjects', 'sub');
				$class_course = $function->getData($class->course_id, 'qa_courses', 'course');
				$teach = $function->getData($class->user_id,'qa_users','user');
				$teach2 = $function->getData($teacher->profile_id,'qa_profile','profile');
    	?>
    
    	<!-- Class Details -->
			<div class="comment-frame col-lg-12">
				<h6>Subject: <br><strong class="margin-1"><?=$class_subject->sub_name;?></strong></h6>
				<h6>Instructor: <br><strong class="margin-1"><?=$teach2->fname;?> <?=($teach2->mname==NULL)?'':$teach2->mname[0].'.';?> <?=$teach2->lname;?></strong></h6>
				<h6>Class: <br><strong class="margin-1"><?=$class_course->course_name;?></strong>
					<?=($class_course->major=="")?"":"<br><strong class='margin-1'>major in ".$class_course->major."</strong>";?>
					<br><strong class="margin-1"><?=$class->year.' section '.$class->section;?></strong></h6>
					<h6>Schedule: <br><strong class="margin-1">Every <?= $class->class_day;?></strong>
					<br><strong class="margin-1"><?='at '.$class->class_time.' in '.$class->class_room;?></strong></h6>
				<!-- <a class="btn btn-danger pull-right" href="#">Remove</a><br><br> -->
			</div>

			<div class="col-lg-12 panel info-body-md">
				<a class="form-control btn btn-default mt-4 form-control" href="classes">ADD new?</a>
				<br><br>
			</div>

    	<?php }else{ ?>

    	<div class="col-lg-12 panel info-body-md">
    		<?php
    			$msg = Session::get("msg");
    			if(isset($msg)){
    				echo $msg;
    				Session::set("msg", NULL);
    			}
    		?>
	    	<div class="panel-body mt-4">

	    
	   		<!-- Add Class -->
	    		<div class="text-center">
					<label style="font-size: 20px; color: grey;"> ADD Class </label>
				</div>
				<form action="navigate?u_id=<?=$user_id;?>" method="post">
					<div class="mt-2">
						<input class="form-control" type="text" name="classcode" placeholder="  Class Code" required="">
					</div>
					<br />
		  				<input class="btn btn-primary pull-right" type="submit" name="add_sub_enrolled" value="ADD">
		  			<br /><br />
				</form>
			<?php } ?>
			</div>
		</div>
	</div>
  </div>

</section>

<?php include ('../../main/footer.php'); ?>