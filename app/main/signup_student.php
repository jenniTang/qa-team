<?php
include '../functions.php';
include_once '../Session.php';
Session::init();
$function = new functions();
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up | Student</title>
    <link rel="icon" href="../lib/qaL.png">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../css/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../css/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom -->
    <link href="../css/custom/custom.min.css" rel="stylesheet">
    <!-- css -->
    <link href="../css/style.css" rel="stylesheet">
</head>


<body class="login" style="background-color: #FFFF99;">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      
      <div class="login_wrapper">
        <div class="animate form login_form">
          <!-- signup for qa_profile && qa_add_info -->
          <section class="login_content">
            <?php
              $msg = Session::get("msg");
              if(isset($msg)){
                echo $msg;
              }
              Session::set("msg",NULL);
              $email = $_GET['email'];
            ?>
            <form action="../navigate?email=<?=$email;?>&utype=1" enctype="multipart/form-data" method="POST">
              <h1>Sign Up</h1><span class="pull-left">1 of 2</span>
              <div><input type="text" class="form-control" placeholder="First Name" name="fname" required=""/></div>
              <div><input type="text" class="form-control" placeholder="Middle Name" name="mname"/></div>
              <div><input type="text" class="form-control" placeholder="Last Name" name="lname" required="" /></div>
              <div class="form-control"><label class="pull-left" style="color: lightgray;"><i>Upload your School I.D. picture here.</i></label>
                <input required="" class="input-group" type="file" name="picture"/></div><br>
              <div class="row">
                <div class="col-lg-12">
                  <select name="course" class="select  col-lg-6" id="qa-form " required="">
                    <option value="">Course</option>
                    <?php
                    $course = $function->getAllData('qa_courses');
                    foreach($course as $value):?>
                    <option value="<?= $value['course_id']?>"><?= $value['course_name']." ".$value['major'];?></option>
                    <?php endforeach; ?>
                  </select>
                  <select name="section" class="select col-lg-6" id="qa-form" required="">
                    <option value="">Section</option>
                    <option>A</option>
                    <option>B</option>
                    <option>C</option>
                    <option>D</option>
                    <option>E</option>
                    <option>F</option>
                  </select>
                </div>
                <div class="col-lg-12">
                  <select name="year" class="select col-lg-6" id="qa-form" required="">
                    <option value="">Year Level</option>
                    <option>First Year</option>
                    <option>Second Year</option>
                    <option>Third Year</option>
                    <option>Fourth Year</option>
                  </select>
                  <select name="status" class="select col-lg-6" id="qa-form" required="">
                    <option value="">Status</option>
                    <option>Regular</option>
                    <option>Irregular</option>
                  </select>
                </div>
              </div>
               
              <div class="pull-right">
               <button id="qa-btn" class="qa-btn-default" type="submit" name="next">Next</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="../login" class="to_register"> Cancel </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="../lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access</h1>
                </div>
              </div>
            </form>
          </section>
        </div>

        <!-- signup for qa_users -->
        <div id="register" class="animate form registration_form">
            <?php
              $email = $_GET['email'];
              $profile_id = $_GET['p_id'];
              $msg2 = Session::get("msg2");
              if(isset($msg2)){
                echo $msg2;
              }
              Session::set("msg2",NULL);
            ?>

          <section class="login_content">
            <form action="../navigate?email=<?=$email;?>&utype=1&p_id=<?=$profile_id;?>" method="post">
              
              <h1>Sign Up</h1><span class="pull-left">2 of 2</span>

              <div>
                <input type="text" class="form-control" placeholder="Email" name="email" readonly="" value="<?=$email;?>" />
              </div>
              <div>
                <h6><i>Password should consist atleast 8 characters with a combination of Numbers, Symbols, Uppercase &amp; Lowercase Letters. </i></h6>
              </div>
              <div >
                <input type="password" class="form-control" placeholder="Password" name="pass" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Confirm Password" name="pass2" required="" />
              </div>

              <div class="pull-right">
                <!-- <a class=" btn-default submit" style="font-size: 20px; margin-right: 0px" href="../login?true=1"> -->
                <button id="qa-btn" class="qa-btn-default" type="submit" name="submit">Submit</button>
               
              </div>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="../login" class="to_register">Cancel</a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="../lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access</h1>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
</body>
</html>