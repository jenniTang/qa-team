<?php
include ('functions.php');
include_once 'Session.php';
  Session::checkLogin(); 
    $function = new Functions();
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>
    <link rel="icon" href="lib/qaL.png">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="css/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom -->
    <link href="css/custom/custom.min.css" rel="stylesheet">
    <!-- css -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="login" style="background-color: #FFFF99;">

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php
              Session::init();
              $msg = Session::get("msg");
              if(isset($msg)){
                echo $msg;
              }
              Session::set("msg",NULL);
              $msg2 = Session::get("msg2");
              if(isset($msg2)){
                echo $msg2;
              }
              Session::set("msg2",NULL);
            ?>
            <form action="navigate" method="post">
              <h1>Login</h1>
              <div>
                <input type="text" class="form-control" placeholder="Email" name="email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
              </div>
              <div class="pull-right">
                <button id="qa-btn" class="qa-btn-default" type="submit" name="login">Log in</button>
               <!--  <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>
                <div class="clearfix"></div>
                <br />

                <div class="">
                  <a class="btn btn-default submit" href="../"><h1 style="margin-top: 20px;"><img src="lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access </h1></a>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h2>Create Account As A?</h2>
              <div>
                <a class="btn btn-default submit" href="signup_email?utype=<?=$function->e(1);?>"><h1 style="margin-top: 20px;">STUDENT</h1></a>
              </div>
              <div>
                <a class="btn btn-default submit" href="signup_email?utype=<?=$function->e(2);?>"><h1 style="margin-top: 20px;">TEACHER</h1></a>
              </div>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <a class="btn btn-default" href="../"><h1 style="margin-top: 20px;"><img src="lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access </h1></a>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
</body>
</html>