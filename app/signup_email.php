<?php
include 'Session.php';
Session::init();
include ('functions.php');
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Email Verification</title>
    <link rel="icon" href="lib/qaL.png">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="css/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom -->
    <link href="css/custom/custom.min.css" rel="stylesheet">
    <!-- css -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="login" style="background-color: #FFFF99;">

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php $utype = $_GET['utype'];
                  $msg = Session::get("msg");
                  if (isset ($msg)) {
                      echo $msg;
                  }
                  Session::set("msg", NULL);
            ?>
            <form action="navigate?utype=<?=$utype;?>" method="post">
              <h1>Verify Email</h1>
              <div>
                <input type="text" class="form-control" placeholder="Email" name="email" required="" />
              </div>
              <div class="pull-right">
                <button id="qa-btn" class="qa-btn-default" type="submit" name="submit_email">submit</button>
              </div>

              <div class="clearfix"></div> 
              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div class="">
                  <a class="btn btn-default submit" href="../"><h1 style="margin-top: 20px;"><img src="lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access </h1></a>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <?php $utype = $_GET['utype'];
                  $email = $_GET['email'];
                  $code = $_GET['code'];

                  $msg2 = Session::get("msg2");
                  if (isset ($msg2)) {
                      echo $msg2;
                  }
                  Session::set("msg2", NULL);
            ?>
            <form action="navigate?utype=<?=$utype;?>&code=<?=$code;?>&email=<?=$email;?>" method="post">
              <h2>Verify Access Code</h2>
              <p>Your mail account will receive an access code. Please check it out to verify here.</p>
              <div>
                <input class="form-control" type="text" name="ucode" placeholder="Access Code Here.">
              </div>
              <div class="pull-right">
                <button id="qa-btn" class="qa-btn-default" type="submit" name="submit_code">submit</button>
              </div>

              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <a class="btn btn-default" href="../"><h1 style="margin-top: 20px;"><img src="lib/qaL-grey.png" height="25" width="30"> NSU CIICT | Quick Access </h1></a>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
</body>
</html>