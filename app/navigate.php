<?php
include 'functions.php';
include_once 'Session.php';
Session::init();
$function = new Functions();

//logging in
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])){
    $usrLogin = $function->getLoginCheck($_POST);
        if(isset($usrLogin)){
	       Session::set("msg2", $usrLogin);
	       header("Location: login");
	    }
    }

//before sign_up
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit_email'])){
	$utype = $_GET['utype'];
	$email = $_POST['email'];
	$flag = $function->checkEmailReg($email);
	if($flag==1){
		header("Location: signup_email?utype=".$utype);
	}
	else{
		$code = rand(100000,900000);
		//mailer2 is still working for template =)
		// header("Location: vendor/mailer3.php?code=".$code."&utype=".$utype."&email=".$email);
		header("Location: signup_email?code=".$code."&utype=".$utype."&email=".$email."#signup");
	}
}


if(isset($_GET['sentmail'])){
	$utype = $_GET['utype'];
	$email = $_GET['email'];
	$code = $_GET['code'];
	if($_GET['sentmail']==1){
		header("Location: signup_email?code=".$code."&utype=".$utype."&email=".$email."#signup");
	}
	if($_GET['sentmail']==0){
		$utype = $_GET['utype'];
		Session::set("msg","<div class='alert alert-danger'><strong>Error ! </strong>Unable to send request!</div>");
		header("Location: signup_email?utype=".$utype);
	}
}

// access code
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit_code'])){
	$utype = $function->d($_GET['utype']);
	$email = $_GET['email'];
	$code = $_GET['code'];
	$ucode = $_POST['ucode'];
 
	if($ucode==$code){
		header("Location: logout?stopcode=1&utype=".$utype."&email=".$email);
	}
	else{
		Session::set("msg2","<div class='alert alert-danger'><strong>Error ! </strong>Invalid Code!</div>");
		header("Location: signup_email?code=".$code."&utype=".$utype."&email=".$email."#signup");
	}
}

if(isset($_GET['stopcode'])){
	$utype = $_GET['utype'];
	$email = $_GET['email'];
	Session::set("msg","<div class='alert alert-success'><strong>Success! </strong>You can sign up now!</div>");
		if($utype==1){
			header("Location: main/signup_student?email=".$email);
		}
		if($utype==2){
			header("Location: main/signup_teacher?email=".$email);
		}
}


if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['next'])){
	$email = $_GET['email'];
	$utype = $_GET['utype'];
	if($utype == 1){
		$add_info_id = $function->addInfo($_POST);
		$profile_id = $function->addProfile2($email, $_POST, $add_info_id);
		header("Location: main/signup_student?email=".$email."&p_id=".$profile_id."#signup");
	}else{
		$profile_id = $function->addProfile2($email, $_POST, 0);
		header("Location: main/signup_teacher?email=".$email."&p_id=".$profile_id."#signup");
	}
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
	$utype = $_GET['utype'];
	$profile_id = $_GET['p_id'];
	$email = $_POST['email'];	
	if($utype == 1){
		$usrRegi = $function->userRegister($_POST, $profile_id, 1);	
	}
	else{
		$usrRegi = $function->userRegister($_POST, $profile_id, 2);
	}
		if(isset($usrRegi)){
			if($usrRegi > 0){
				Session::set("msg","<div class='alert alert-success mt-4'><strong>Success! </strong>Please wait until the admin verify your account.</div>");
				header("Location: login");
			}
			else{
				Session::set("msg2", $usrRegi);
				if($utype==1){
					header("Location: main/signup_student?email=".$email."&p_id=".$profile_id."#signup");
				}
				else{
					header("Location: main/signup_teacher?email=".$email."&p_id=".$profile_id."#signup");
				}
			}
		}
}

if(isset($_GET['false'])){
	session_start();
	Session::init();
	Session::set("msg","<div class='alert alert-success'><strong>Successfully logged out! </strong></div>");
	header("Location: login");
}

?>